<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function list(): \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application
    {
        return view('tests-categories.list');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $category = new Category;

        return view('tests-categories.create', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Category $category)
    {
        if (Auth::user()->can('update', $category)) {
            return view('tests-patient-procedures.edit', compact('category'));
        } else {
            abort(403, __('messages.unauthorized_edit_test'));
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Category $category)
    {
        if (Auth::user()->can('delete', $category)) {
            $category->delete();

            return redirect()->route('categories.list')->with('success', __('messages.category_deleted'));
        } else {
            abort(403, __('messages.unauthorized_delete_test'));
        }
    }
}
