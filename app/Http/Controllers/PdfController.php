<?php

namespace App\Http\Controllers;

use Barryvdh\DomPDF\Facade\Pdf;

class PdfController extends Controller
{
    public function generate(): \Illuminate\Http\Response
    {
        $data = [
            'name' => 'ItSolutionStuff.com',
            'email' => 'toto@free.fr',
            'website' => 'https://www.itsolutionstuff.com',
            'logo' => public_path('logo.png'),
            'phone' => '123456789',
            'title' => 'Welcome to ItSolutionStuff.com',
            'date' => date('m/d/Y'),
        ];
        $pdf = PDF::loadView('pdf.template', $data);

        return $pdf->download('itsolutionstuff.pdf');
    }
}
