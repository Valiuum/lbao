<?php

namespace App\Http\Controllers;

use App\Models\Test;
use Illuminate\Support\Facades\Auth;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function list(): \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application
    {
        return view('tests-patient-procedures.list');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        if (Auth::user()->can('create', Test::class)) {
            return view('tests-patient-procedures.create');
        } else {
            abort(403, __('messages.unauthorized_create_test'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Test $test)
    {
        if (Auth::user()->can('update', $test)) {
            return view('tests-patient-procedures.edit', compact('test'));
        } else {
            abort(403, __('messages.unauthorized_edit_test'));
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Test $test)
    {
        if (Auth::user()->can('delete', $test)) {
            $test->delete();

            return redirect()->route('test.list')->with('success', __('messages.test_deleted'));
        } else {
            abort(403, __('messages.unauthorized_delete_test'));
        }
    }
}
