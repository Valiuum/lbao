<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class testsCompleted
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {

        if (session()->has('patient') && ! empty(session('patient.informations')) && ! empty(session('patient.testList'))) {

            foreach (session('patient.testList') as $test) {
                if (! $test['completed']) {
                    return redirect('dashboard')->with('warning', trans('messages.unauthorized_generate_bilan'));
                }
            }

            return $next($request);
        }

        return redirect('dashboard')->with('warning', trans('messages.unauthorized_generate_tests'));
    }
}
