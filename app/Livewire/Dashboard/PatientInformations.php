<?php

namespace App\Livewire\Dashboard;

use Livewire\Attributes\On;
use Livewire\Attributes\Session;
use Livewire\Component;

class PatientInformations extends Component
{
    #[Session(key: 'patient.informations')]
    public array $patientInformations;

    public function openModal()
    {
        $this->dispatch('open-modal-patient', $this->patientInformations ?? null);
    }

    #[On('clear-patient-informations')]
    public function clearPatientInformations()
    {
        $this->patientInformations = [];
    }

    #[On('close-modal-patient')]
    public function closeModal()
    {
        $this->patientInformations = session('patient.informations');
    }

    #[On('check-patient-informations')]
    public function checkPatientInformations(): void
    {
        if (empty($this->patientInformations)) {
            $this->dispatch('notify',
                ['message' => trans('messages.patient_missing_infos'), 'type' => 'warning']
            );
        }
    }

    public function render()
    {
        return view('livewire.dashboard.patient-informations');
    }
}
