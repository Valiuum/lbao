<?php

namespace App\Livewire\Dashboard;

use App\Models\Test;
use Livewire\Attributes\Locked;
use Livewire\Attributes\On;
use Livewire\Attributes\Session;
use Livewire\Component;

class PatientTests extends Component
{
    #[Session(key: 'patient.testsList')]
    public array $patientTestsList;

    #[Locked]
    public bool $patientProfileCompleted = false;

    #[On('clear-patient-informations')]
    public function patientProfileCompletedEmpty()
    {
        $this->patientProfileCompleted = false;
    }

    public function mount(): void
    {
        if (session()->has('patient.informations') && ! empty(session('patient.informations'))) {
            $this->patientProfileCompleted = true;
        }
    }

    #[On('close-modal-patient')]
    public function patientProfileCompletedSuccess()
    {
        $this->patientProfileCompleted = true;
    }

    public function loadNewItem($itemId): void
    {

        $item = Test::find($itemId, ['name']);

        if (! $item) {

            $this->dispatch('notify',
                ['message' => trans('messages.unable_to_find_test'), 'type' => 'danger']
            );

            return;

        }

        $testName = $item->name;

        $componentName = 'TestsPatient.'.ucfirst($testName);
        $componentPath = app_path('Livewire/'.str_replace('.', '/', $componentName).'.php');

        if (! file_exists($componentPath)) {
            $this->dispatch('notify',
                [
                    'message' => trans_choice('messages.unable_to_load_file', 1, ['value' => $testName]),
                    'type' => 'danger',
                ]
            );

            return;
        }

        $uuid = uniqid('item_');
        $this->patientTestsList[] = [
            'id' => $itemId,
            'uuid' => $uuid,
            'position' => collect($this->patientTestsList)->count(),
            'name' => $testName,
            'is_completed' => false,
            'data' => [
                'form' => [],
                'result' => [],
            ],
        ];

        session(['patient.testsList' => $this->patientTestsList]);

        $this->dispatch('add-item', [$uuid]);

    }

    #[On('updated-testList')]
    public function updatedTestList($test): void
    {
        $this->patientTestsList = collect($this->patientTestsList)
            ->map(function ($item) use ($test) {
                if ($item['uuid'] === $test['uuid']) {
                    $item['data'] = $test['data'];
                    $item['is_completed'] = $test['is_completed'];
                }

                return $item;
            })
            ->toArray();
        session(['patient.testsList' => $this->patientTestsList]);
    }

    public function sortItem($item, $newPosition): void
    {
        $currentPosition = collect($this->patientTestsList)->firstWhere('uuid', $item)['position'];
        $this->patientTestsList = collect(session('patient.testsList'))
            ->map(function ($test) use ($currentPosition, $newPosition, $item) {
                if ($test['uuid'] === $item) {
                    $test['position'] = $newPosition;
                } elseif ($test['position'] >= min($currentPosition,
                    $newPosition) && $test['position'] <= max($currentPosition, $newPosition)) {
                    $test['position'] += ($currentPosition < $newPosition) ? -1 : 1;
                }

                return $test;
            })
            ->sortBy('position')
            ->values()
            ->toArray();
        session(['patient.testsList' => $this->patientTestsList]);
    }

    public function removeItem($uuid): void
    {

        $currentPosition = collect($this->patientTestsList)->firstWhere('uuid', $uuid)['position'];

        $this->patientTestsList = collect(session('patient.testsList'))
            ->map(function ($test) use ($currentPosition, $uuid) {
                if ($test['uuid'] === $uuid) {
                    return null;
                }
                if ($test['position'] > $currentPosition) {
                    $test['position'] -= 1;
                }

                return $test;
            })
            ->filter()
            ->values()
            ->toArray();
        session(['patient.testsList' => $this->patientTestsList]);
        $this->dispatch('remove-item', [$uuid]);

    }

    public function checkPatientTestsCompleted(): bool
    {

        $incompleteTests = collect($this->patientTestsList)->where('is_completed', false);

        if (! $this->patientProfileCompleted) {

            $this->dispatch('notify',
                ['message' => trans('messages.patient_missing_infos'), 'type' => 'warning']
            );

            return false;
        }

        if (collect($this->patientTestsList)->count() === 0) {

            $this->dispatch('notify',
                ['message' => trans('messages.no_test_selected'), 'type' => 'warning']

            );

            return false;

        } elseif ($incompleteTests->count() > 0) {

            $errors = $incompleteTests->map(function ($test) {
                return $test['name'];
            });

            $this->dispatch('notify',
                [
                    'message' => trans_choice('messages.test_validation_failed', $incompleteTests->count(),
                        ['values' => $errors->implode(', ')]), 'type' => 'warning',
                ]
            );

            return false;

        }

        return true;

    }

    public function submitItems(): void
    {

        if (! $this->checkPatientTestsCompleted()) {
            return;
        }

        dd('All tests are completed');

    }

    public function render()
    {
        return view('livewire.dashboard.patient-tests');
    }
}
