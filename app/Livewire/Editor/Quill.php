<?php

namespace App\Livewire\Editor;

use App\Models\Test;
use Livewire\Attributes\On;
use Livewire\Component;

class Quill extends Component
{
    public string $content;

    public string $quillId;

    public string $quillReadOnly;

    public string $quillToolBar;

    public function mount($quillReadOnly = 'false', $content = null)
    {
        $this->quillReadOnly = $quillReadOnly;
        $this->quillToolBar = $this->quillReadOnly === 'true' ? 'false' : 'true';
        $this->quillId = 'quill-'.uniqid();

        if ($content) {
            $this->content = $content;
        }
    }

    #[On('load-item-description')]
    public function loadItemDescription($data): void
    {
        $item = Test::find($data['id'], ['content']);

        if (! $item) {

            $this->dispatch('notify',
                ['message' => trans('messages.unable_to_find_test'), 'type' => 'danger']
            );

            return;

        }

        $this->content = $item->content;
        $this->dispatch('quill-content-update', [$this->content]);
    }

    public function updatedContent($content)
    {
        $this->dispatch('quill-content-update', $content);
        // $this->content = $content;
    }

    public function render()
    {
        return view('livewire.editor.quill');
    }
}
