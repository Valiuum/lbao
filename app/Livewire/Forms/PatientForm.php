<?php

namespace App\Livewire\Forms;

use Illuminate\Validation\Rule;
use Livewire\Form;

class PatientForm extends Form
{
    public string $name = '';

    public string $username = '';

    public string $birthdate = '';

    public string $job = '';

    public array $school_grade_options = [
        '1',
        '2',
        '3',
    ];

    public string $school_grade = '';

    public array $gender_options = [
        'male',
        'female',
    ];

    public string $gender = '';

    public string $date = '';

    public function rules(): array
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'username' => ['required', 'string', 'min:1', 'max:255'],
            'birthdate' => ['required', 'date', 'date_format:"Y-m-d"'],
            'job' => ['required', 'string', 'max:255'],
            'date' => ['required', 'date', 'date_format:"Y-m-d"'],
            'gender' => ['required', 'string', Rule::in($this->gender_options)],
            'school_grade' => ['required', 'string', Rule::in($this->school_grade_options)],
        ];
    }
}
