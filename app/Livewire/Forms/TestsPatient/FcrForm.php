<?php

namespace App\Livewire\Forms\TestsPatient;

use Livewire\Form;

class FcrForm extends Form
{
    public array $fcr = [
        'figure_a' => [
            'copie' => [
                'figure_1' => ['score' => 0],
                'figure_2' => ['score' => 0],
                'figure_3' => ['score' => 0],
                'figure_4' => ['score' => 0],
                'figure_5' => ['score' => 0],
                'figure_6' => ['score' => 0],
                'figure_7' => ['score' => 0],
                'figure_8' => ['score' => 0],
                'figure_9' => ['score' => 0],
                'figure_10' => ['score' => 0],
                'figure_11' => ['score' => 0],
                'figure_12' => ['score' => 0],
                'figure_13' => ['score' => 0],
                'figure_14' => ['score' => 0],
                'figure_15' => ['score' => 0],
                'figure_16' => ['score' => 0],
                'figure_17' => ['score' => 0],
                'figure_18' => ['score' => 0],
                'total_score' => 0,
                'type_c' => '',
                'time' => '',
                'height' => '',
                'width' => '',
                'comment' => '',
            ],
            'reproduction' => [
                'figure_1' => ['score' => 0],
                'figure_2' => ['score' => 0],
                'figure_3' => ['score' => 0],
                'figure_4' => ['score' => 0],
                'figure_5' => ['score' => 0],
                'figure_6' => ['score' => 0],
                'figure_7' => ['score' => 0],
                'figure_8' => ['score' => 0],
                'figure_9' => ['score' => 0],
                'figure_10' => ['score' => 0],
                'figure_11' => ['score' => 0],
                'figure_12' => ['score' => 0],
                'figure_13' => ['score' => 0],
                'figure_14' => ['score' => 0],
                'figure_15' => ['score' => 0],
                'figure_16' => ['score' => 0],
                'figure_17' => ['score' => 0],
                'figure_18' => ['score' => 0],
                'total_score' => 0,
                'type_r' => '',
                'time' => '',
                'height' => '',
                'width' => '',
                'comment' => '',
            ],
            'results' => [
                'copie' => [
                    'score' => [
                        'value' => 0,
                        'percentile' => 0,
                        'table_reference' => 'table_a1'
                    ],
                    'time' => [
                        'value' => '',
                        'percentile' => 0,
                        'table_reference' => 'table_a3'
                    ],
                    'width' => [
                        'value' => 0,
                        'percentile' => 0,
                        'table_reference' => 'table_a5'
                    ],
                    'height' => [
                        'value' => 0,
                        'percentile' => 0,
                        'table_reference' => 'table_a7'
                    ],
                    'type' => ''
                ],
                'reproduction' => [
                    'score' => [
                        'value' => 0,
                        'percentile' => 0,
                        'table_reference' => 'table_a2'
                    ],
                    'time' => [
                        'value' => '',
                        'percentile' => 0,
                        'table_reference' => 'table_a4'
                    ],
                    'width' => [
                        'value' => 0,
                        'percentile' => 0,
                        'table_reference' => 'table_a6'
                    ],
                    'height' => [
                        'value' => 0,
                        'percentile' => 0,
                        'table_reference' => 'table_a8'
                    ],
                    'type' => ''
                ]
            ]
        ],
        'figure_b' => [
            'copie' => [],
            'reproduction' => []
        ],
    ];

    public string $version = 'A';

    public string $comment = '';

    public string $time = '';

    public function validateFigure(): array
    {
        $version = $this->version;
        if ($version === 'A') {
            return $this->validateFigureA();
        } else {
            return $this->validateFigureB();
        }
    }

    private function validateFigureA(): array
    {
        return [
            'comment' => ['nullable', 'string', 'max:255'],
        ];
    }

    private function validateFigureB(): array
    {
        return [
            'comment' => ['nullable', 'string', 'max:255'],
        ];
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return $this->validateFigure();
    }

}
