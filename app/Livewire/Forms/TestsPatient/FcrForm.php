<?php

namespace App\Livewire\Forms\TestsPatient;

use Livewire\Form;

class FcrForm extends Form
{
    public array $fcr = [
        'figure_a' => [
            'copie' => [
                'fcr_1' => [
                    'score' => '',
                ],
                'fcr_2' => [
                    'score' => '',
                ],
                'fcr_3' => [
                    'score' => '',
                ],
                'fcr_4' => [
                    'score' => '',
                ],
                'fcr_5' => [
                    'score' => '',
                ],
                'fcr_6' => [
                    'score' => '',
                ],
                'fcr_7' => [
                    'score' => '',
                ],
                'fcr_8' => [
                    'score' => '',
                ],
                'fcr_9' => [
                    'score' => '',
                ],
                'fcr_10' => [
                    'score' => '',
                ],
                'fcr_11' => [
                    'score' => '',
                ],
                'fcr_12' => [
                    'score' => '',
                ],
                'fcr_13' => [
                    'score' => '',
                ],
                'fcr_14' => [
                    'score' => '',
                ],
                'fcr_15' => [
                    'score' => '',
                ],
                'fcr_16' => [
                    'score' => '',
                ],
                'fcr_17' => [
                    'score' => '',
                ],
                'fcr_18' => [
                    'score' => '',
                ],
                'total_score' => 0,
                'type_c' => '',
                'time' => '',
                'height' => '',
                'width' => '',
                'comment' => '',
            ],
            'reproduction' => [
                'fcr_1' => [
                    'score' => '',
                ],
                'fcr_2' => [
                    'score' => '',
                ],
                'fcr_3' => [
                    'score' => '',
                ],
                'fcr_4' => [
                    'score' => '',
                ],
                'fcr_5' => [
                    'score' => '',
                ],
                'fcr_6' => [
                    'score' => '',
                ],
                'fcr_7' => [
                    'score' => '',
                ],
                'fcr_8' => [
                    'score' => '',
                ],
                'fcr_9' => [
                    'score' => '',
                ],
                'fcr_10' => [
                    'score' => '',
                ],
                'fcr_11' => [
                    'score' => '',
                ],
                'fcr_12' => [
                    'score' => '',
                ],
                'fcr_13' => [
                    'score' => '',
                ],
                'fcr_14' => [
                    'score' => '',
                ],
                'fcr_15' => [
                    'score' => '',
                ],
                'fcr_16' => [
                    'score' => '',
                ],
                'fcr_17' => [
                    'score' => '',
                ],
                'fcr_18' => [
                    'score' => '',
                ],
                'total_score' => 0,
                'type_r' => '',
                'time' => '',
                'height' => '',
                'width' => '',
                'comment' => '',
            ],
            'fcr_results' => [
                'copie' => [
                    'score' => [
                        'value' => 0,
                        'percentile' => 0,
                        'table_reference' => 'table_a1'
                    ],
                    'time' => [
                        'value' => '',
                        'percentile' => 0,
                        'table_reference' => 'table_a3'
                    ],
                    'width' => [
                        'value' => 0,
                        'percentile' => 0,
                        'table_reference' => 'table_a5'
                    ],
                    'height' => [
                        'value' => 0,
                        'percentile' => 0,
                        'table_reference' => 'table_a7'
                    ],
                    'type' => ''
                ],
                'reproduction' => [
                    'score' => [
                        'value' => 0,
                        'percentile' => 0,
                        'table_reference' => 'table_a2'
                    ],
                    'time' => [
                        'value' => '',
                        'percentile' => 0,
                        'table_reference' => 'table_a4'
                    ],
                    'width' => [
                        'value' => 0,
                        'percentile' => 0,
                        'table_reference' => 'table_a6'
                    ],
                    'height' => [
                        'value' => 0,
                        'percentile' => 0,
                        'table_reference' => 'table_a8'
                    ],
                    'type' => ''
                ]
            ]
        ],
        'figure_b' => [
            'copie' => [

            ],
            'reproduction' => [

            ]
        ],

    ];
}
