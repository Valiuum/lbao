<?php

namespace App\Livewire\Forms\TestsPatient;

use Livewire\Form;

class McstForm extends Form
{
    public array $mcst = [
        'mcst_1' => [
            'position' => 1,
            'card' => '2RR',
            'criteria' => '',
            'criteria_reason' => '',
            'answer' => '',
        ],
        'mcst_2' => [
            'position' => 2,
            'card' => '4EJ',
            'criteria' => '',
            'criteria_reason' => '',
            'answer' => '',
        ],
        'mcst_3' => [
            'position' => 3,
            'card' => '3TV',
            'criteria' => '',
            'criteria_reason' => '',
            'answer' => '',
        ],
        'mcst_4' => [
            'position' => 4,
            'card' => '1CB',
            'criteria' => '',
            'criteria_reason' => '',
            'answer' => '',
        ],
        'mcst_5' => [
            'position' => 5,
            'card' => '3ER',
            'criteria' => '',
            'criteria_reason' => '',
            'answer' => '',
        ],
        'mcst_6' => [
            'position' => 6,
            'card' => '2TB',
            'criteria' => '',
            'criteria_reason' => '',
            'answer' => '',
        ],
        'mcst_7' => [
            'position' => 7,
            'card' => '1RJ',
            'criteria' => '',
            'criteria_reason' => '',
            'answer' => '',
        ],
        'mcst_8' => [
            'position' => 8,
            'card' => '4CV',
            'criteria' => '',
            'criteria_reason' => '',
            'answer' => '',
        ],
        'mcst_9' => [
            'position' => 9,
            'card' => '2TJ',
            'criteria' => '',
            'criteria_reason' => '',
            'answer' => '',
        ],
        'mcst_10' => [
            'position' => 10,
            'card' => '1EB',
            'criteria' => '',
            'criteria_reason' => '',
            'answer' => '',
        ],
        'mcst_11' => [
            'position' => 11,
            'card' => '3RV',
            'criteria' => '',
            'criteria_reason' => '',
            'answer' => '',
        ],
        'mcst_12' => [
            'position' => 12,
            'card' => '4CR',
            'criteria' => '',
            'criteria_reason' => '',
            'answer' => '',
        ],
        'mcst_13' => [
            'position' => 13,
            'card' => '3TB',
            'criteria' => '',
            'criteria_reason' => '',
            'answer' => '',
        ],
        'mcst_14' => [
            'position' => 14,
            'card' => '2RJ',
            'criteria' => '',
            'criteria_reason' => '',
            'answer' => '',
        ],
        'mcst_15' => [
            'position' => 15,
            'card' => '4ER',
            'criteria' => '',
            'criteria_reason' => '',
            'answer' => '',
        ],
        'mcst_16' => [
            'position' => 16,
            'card' => '1CV',
            'criteria' => '',
            'criteria_reason' => '',
            'answer' => '',
        ],
        'mcst_17' => [
            'position' => 17,
            'card' => '3EB',
            'criteria' => '',
            'criteria_reason' => '',
            'answer' => '',
        ],
        'mcst_18' => [
            'position' => 18,
            'card' => '1RV',
            'criteria' => '',
            'criteria_reason' => '',
            'answer' => '',
        ],
        'mcst_19' => [
            'position' => 19,
            'card' => '2CR',
            'criteria' => '',
            'criteria_reason' => '',
            'answer' => '',
        ],
        'mcst_20' => [
            'position' => 20,
            'card' => '4TJ',
            'criteria' => '',
            'criteria_reason' => '',
            'answer' => '',
        ],
        'mcst_21' => [
            'position' => 21,
            'card' => '3RR',
            'criteria' => '',
            'criteria_reason' => '',
            'answer' => '',
        ],
        'mcst_22' => [
            'position' => 22,
            'card' => '2CB',
            'criteria' => '',
            'criteria_reason' => '',
            'answer' => '',
        ],
        'mcst_23' => [
            'position' => 23,
            'card' => '4TV',
            'criteria' => '',
            'criteria_reason' => '',
            'answer' => '',
        ],
        'mcst_24' => [
            'position' => 24,
            'card' => '1EJ',
            'criteria' => '',
            'criteria_reason' => '',
            'answer' => '',
        ],
        'mcst_25' => [
            'position' => 25,
            'card' => '2RR',
            'criteria' => '',
            'criteria_reason' => '',
            'answer' => '',
        ],
        'mcst_26' => [
            'position' => 26,
            'card' => '4EJ',
            'criteria' => '',
            'criteria_reason' => '',
            'answer' => '',
        ],
        'mcst_27' => [
            'position' => 27,
            'card' => '3TV',
            'criteria' => '',
            'criteria_reason' => '',
            'answer' => '',
        ],
        'mcst_28' => [
            'position' => 28,
            'card' => '1CB',
            'criteria' => '',
            'criteria_reason' => '',
            'answer' => '',
        ],
        'mcst_29' => [
            'position' => 29,
            'card' => '3ER',
            'criteria' => '',
            'criteria_reason' => '',
            'answer' => '',
        ],
        'mcst_30' => [
            'position' => 30,
            'card' => '2TB',
            'criteria' => '',
            'criteria_reason' => '',
            'answer' => '',
        ],
        'mcst_31' => [
            'position' => 31,
            'card' => '1RJ',
            'criteria' => '',
            'criteria_reason' => '',
            'answer' => '',
        ],
        'mcst_32' => [
            'position' => 32,
            'card' => '4CV',
            'criteria' => '',
            'criteria_reason' => '',
            'answer' => '',
        ],
        'mcst_33' => [
            'position' => 33,
            'card' => '2TJ',
            'criteria' => '',
            'criteria_reason' => '',
            'answer' => '',
        ],
        'mcst_34' => [
            'position' => 34,
            'card' => '1EB',
            'criteria' => '',
            'criteria_reason' => '',
            'answer' => '',
        ],
        'mcst_35' => [
            'position' => 35,
            'card' => '3RV',
            'criteria' => '',
            'criteria_reason' => '',
            'answer' => '',
        ],
        'mcst_36' => [
            'position' => 36,
            'card' => '4CR',
            'criteria' => '',
            'criteria_reason' => '',
            'answer' => '',
        ],
        'mcst_37' => [
            'position' => 37,
            'card' => '3TB',
            'criteria' => '',
            'criteria_reason' => '',
            'answer' => '',
        ],
        'mcst_38' => [
            'position' => 38,
            'card' => '2RJ',
            'criteria' => '',
            'criteria_reason' => '',
            'answer' => '',
        ],
        'mcst_39' => [
            'position' => 39,
            'card' => '4ER',
            'criteria' => '',
            'criteria_reason' => '',
            'answer' => '',
        ],
        'mcst_40' => [
            'position' => 40,
            'card' => '1CV',
            'criteria' => '',
            'criteria_reason' => '',
            'answer' => '',
        ],
        'mcst_41' => [
            'position' => 41,
            'card' => '3EB',
            'criteria' => '',
            'criteria_reason' => '',
            'answer' => '',
        ],
        'mcst_42' => [
            'position' => 42,
            'card' => '1RV',
            'criteria' => '',
            'criteria_reason' => '',
            'answer' => '',
        ],
        'mcst_43' => [
            'position' => 43,
            'card' => '2CR',
            'criteria' => '',
            'criteria_reason' => '',
            'answer' => '',
        ],
        'mcst_44' => [
            'position' => 44,
            'card' => '4TJ',
            'criteria' => '',
            'criteria_reason' => '',
            'answer' => '',
        ],
        'mcst_45' => [
            'position' => 45,
            'card' => '3RR',
            'criteria' => '',
            'criteria_reason' => '',
            'answer' => '',
        ],
        'mcst_46' => [
            'position' => 46,
            'card' => '2CB',
            'criteria' => '',
            'criteria_reason' => '',
            'answer' => '',
        ],
        'mcst_47' => [
            'position' => 47,
            'card' => '4TV',
            'criteria' => '',
            'criteria_reason' => '',
            'answer' => '',
        ],
        'mcst_48' => [
            'position' => 48,
            'card' => '1EJ',
            'criteria' => '',
            'criteria_reason' => '',
            'answer' => '',
        ],
    ];

    public array $mcst_results = [
        'nbr_categories_completed' => [
            'score' => 6,
            'comment' => '',
            'moyenne' => 0,
            'ecart_type' => 0,
            'deviation' => 0,
        ],
        'nbr_errors_no' => [
            'score' => 1,
            'comment' => '',
            'moyenne' => 0,
            'ecart_type' => 0,
            'deviation' => 0,
        ],
        'nbr_perseverations' => [
            'score' => 0,
            'comment' => '',
            'moyenne' => 0,
            'ecart_type' => 0,
            'deviation' => 0,
        ],
        'percentage_perseverations' => [
            'score' => 0,
        ],
        'nbr_cards_six_cat' => [
            'score' => 37,
        ],
        'nbr_dropouts_premature' => [
            'score' => 0,
        ],
    ];

    public string $comment = '';

    public string $time = '';

    public function rules(): array
    {
        return [
            'mcst.*.criteria' => ['required', 'string'],
            'mcst.*.criteria_reason' => ['nullable', 'string'],
            'mcst.*.answer' => ['required', 'string'],
            'mcst_results.nbr_categories_completed.score' => ['required', 'numeric'],
            'mcst_results.nbr_errors_no.score' => ['required', 'numeric'],
            'mcst_results.nbr_perseverations.score' => ['required', 'numeric'],
            'mcst_results.percentage_perseverations.score' => ['required', 'numeric'],
            'mcst_results.nbr_cards_six_cat.score' => ['required', 'numeric'],
            'mcst_results.nbr_dropouts_premature.score' => ['required', 'numeric'],
            'comment' => ['nullable', 'string', 'max:255'],
            'time' => ['required', 'date_format:H:i:s'],
        ];
    }

    public function messages(): array
    {
        $messages = [];
        foreach ($this->mcst as $index => $value) {
            $messages["mcst.{$index}.criteria.required"] = trans_choice('messages.mcst_criteria_validation_error', 1,
                ['value' => $value['card']]);
            $messages["mcst.{$index}.answer.required"] = trans_choice('messages.mcst_answer_validation_error', 1,
                ['value' => $value['card']]);
        }

        foreach ($this->mcst_results as $index => $value) {
            $messages["mcst_results.{$index}.score.required"] = trans_choice('messages.mcst_score_validation_error', 1,
                ['value' => trans('messages.mcst_'.$index)]);
        }

        return $messages;
    }
}
