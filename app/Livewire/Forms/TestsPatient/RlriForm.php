<?php

namespace App\Livewire\Forms\TestsPatient;

use Livewire\Form;

class RlriForm extends Form
{
    public string $username = 'toto';

    public string $password = '1234';

    public int $value = 0;

    public function rules(): array
    {
        return [
            'email' => 'required|string|email',
        ];
    }
}
