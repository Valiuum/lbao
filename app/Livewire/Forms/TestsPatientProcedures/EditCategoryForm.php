<?php

namespace App\Livewire\Forms\TestsPatientProcedures;

use Livewire\Form;

class EditCategoryForm extends Form
{
    public string $name = '';

    public string $slug = '';

    public int $id = 0;

    public function rules(): array
    {
        return [
            'name' => ['required', 'string', 'max:255', 'unique:categories,name,'.$this->id],
            'slug' => ['required', 'string', 'max:255'],
        ];
    }
}
