<?php

namespace App\Livewire\Forms\TestsPatientProcedures;

use Livewire\Form;

class EditProcedureForm extends Form
{
    public string $name = '';

    public string $slug = '';

    public string $versions = '';

    public string $content = '';

    public bool $is_active = true;

    public int $id = 0;

    public array $categories = [];

    public array $users = [];

    public function rules(): array
    {
        return [
            'categories' => ['required', 'array', 'min:1'],
            'users' => ['required', 'array', 'min:1'],
            'name' => ['required', 'string', 'max:255', 'unique:tests,name,'.$this->id],
            'slug' => ['required', 'string', 'max:255'],
            // 'versions' => ['required', 'string', 'regex:/^\["[^"]*"(?:,"[^"]*")*\]$/'],
            'versions' => ['required', 'string', 'regex:/^\[(?:"[^"]*"(?:,"[^"]*")*)?\]$/'],
            'content' => ['nullable', 'string'],
            'is_active' => ['boolean'],
        ];
    }
}
