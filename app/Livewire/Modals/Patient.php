<?php

namespace App\Livewire\Modals;

use App\Livewire\Forms\PatientForm;
use Livewire\Attributes\On;
use Livewire\Component;

class Patient extends Component
{
    public PatientForm $form;

    public bool $isOpen = false;

    #[On('open-modal-patient')]
    public function openModal($data = null)
    {

        if ($data) {
            $this->form->fill($data);
        }

        $this->isOpen = true;
    }

    public function save()
    {
        $this->form->date = now()->format('Y-m-d');
        $this->validate();
        $data = $this->form->except('gender_options', 'school_grade_options');
        $this->store($data);
        $this->dispatch('close-modal-patient');
        $this->dispatch('notify',
            ['message' => trans('messages.modal_patient_info_added'), 'type' => 'success']
        );
        $this->form->reset();
    }

    public function store($data)
    {

        if (session()->has('patient.informations')) {
            session()->forget('patient.informations');
        }

        session()->put('patient.informations', $data);
        $this->isOpen = false;

    }

    public function clearForm()
    {
        if (session()->has('patient.informations')) {
            session()->forget('patient.informations');
        }
        $this->form->reset();
        $this->dispatch('clear-patient-informations');
    }

    public function render()
    {
        return view('livewire.modals.patient');
    }
}
