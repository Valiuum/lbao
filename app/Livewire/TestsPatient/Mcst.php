<?php

namespace App\Livewire\TestsPatient;

use App\Livewire\Forms\TestsPatient\McstForm;
use App\Models\Test;
use Carbon\Carbon;
use Livewire\Attributes\Locked;
use Livewire\Component;

class Mcst extends Component
{
    #[Locked]
    public string $uuid;

    #[Locked]
    public string $name = 'mcst';

    public Test $test;

    public McstForm $form;

    private array $normes = [

        'categories' => [
            '<40' => [
                1 => [
                    5 => 4.1,
                    10 => 5,
                    25 => 6,
                    50 => 6,
                    75 => 6,
                    90 => 6,
                    95 => 6,
                    'moyenne' => 5.75,
                    'ecart_type' => .65,
                ],
                2 => [
                    5 => 4,
                    10 => 5,
                    25 => 6,
                    50 => 6,
                    75 => 6,
                    90 => 6,
                    95 => 6,
                    'moyenne' => 5.92,
                    'ecart_type' => .36,
                ],
                3 => [
                    5 => 5,
                    10 => 5,
                    25 => 6,
                    50 => 6,
                    75 => 6,
                    90 => 6,
                    95 => 6,
                    'moyenne' => 5.93,
                    'ecart_type' => .38,
                ],
            ],
            '40-59' => [
                1 => [
                    5 => 2.1,
                    10 => 4,
                    25 => 5,
                    50 => 6,
                    75 => 6,
                    90 => 6,
                    95 => 6,
                    'moyenne' => 5.74,
                    'ecart_type' => .67,
                ],
                2 => [
                    5 => 3.8,
                    10 => 4.6,
                    25 => 6,
                    50 => 6,
                    75 => 6,
                    90 => 6,
                    95 => 6,
                    'moyenne' => 5.95,
                    'ecart_type' => .22,
                ],
                3 => [
                    5 => 4.45,
                    10 => 5,
                    25 => 6,
                    50 => 6,
                    75 => 6,
                    90 => 6,
                    95 => 6,
                    'moyenne' => 5.84,
                    'ecart_type' => .53,
                ],
            ],
            '>=60' => [
                1 => [
                    5 => 2,
                    10 => 3,
                    25 => 5,
                    50 => 6,
                    75 => 6,
                    90 => 6,
                    95 => 6,
                    'moyenne' => 5.27,
                    'ecart_type' => 1.08,
                ],
                2 => [
                    5 => 2,
                    10 => 3,
                    25 => 5,
                    50 => 6,
                    75 => 6,
                    90 => 6,
                    95 => 6,
                    'moyenne' => 5.84,
                    'ecart_type' => .46,
                ],
                3 => [
                    5 => 4,
                    10 => 4,
                    25 => 6,
                    50 => 6,
                    75 => 6,
                    90 => 6,
                    95 => 6,
                    'moyenne' => 5.71,
                    'ecart_type' => .96,
                ],
            ],
        ],

        'total_errors' => [
            '<40' => [
                1 => [
                    5 => 0,
                    10 => 0,
                    25 => 0,
                    50 => 1,
                    75 => 5.25,
                    90 => 9.9,
                    95 => 18.5,
                    'moyenne' => 3.18,
                    'ecart_type' => 5.12,
                ],
                2 => [
                    5 => 0,
                    10 => 0,
                    25 => 1.5,
                    50 => 2,
                    75 => 7,
                    90 => 12,
                    95 => 15,
                    'moyenne' => 3.19,
                    'ecart_type' => 3.04,
                ],
                3 => [
                    5 => 0,
                    10 => 0,
                    25 => 1,
                    50 => 3,
                    75 => 6,
                    90 => 9,
                    95 => 12,
                    'moyenne' => 2.84,
                    'ecart_type' => 3.5,
                ],
            ],
            '40-59' => [
                1 => [
                    5 => 0,
                    10 => .2,
                    25 => 2,
                    50 => 5,
                    75 => 9,
                    90 => 16.4,
                    95 => 25.8,
                    'moyenne' => 4.32,
                    'ecart_type' => 3.43,
                ],
                2 => [
                    5 => 0,
                    10 => .6,
                    25 => 2,
                    50 => 4,
                    75 => 7,
                    90 => 12.8,
                    95 => 19.6,
                    'moyenne' => 3.73,
                    'ecart_type' => 2.66,
                ],
                3 => [
                    5 => 0,
                    10 => 0,
                    25 => 1,
                    50 => 3,
                    75 => 6.75,
                    90 => 13,
                    95 => 16.1,
                    'moyenne' => 3.75,
                    'ecart_type' => 3.72,
                ],
            ],
            '>=60' => [
                1 => [
                    5 => 0,
                    10 => 0,
                    25 => 4,
                    50 => 7,
                    75 => 12,
                    90 => 25.4,
                    95 => 29.8,
                    'moyenne' => 7.09,
                    'ecart_type' => 6.08,
                ],
                2 => [
                    5 => 0,
                    10 => 0,
                    25 => 3,
                    50 => 5.5,
                    75 => 10,
                    90 => 24,
                    95 => 30.5,
                    'moyenne' => 4.71,
                    'ecart_type' => 3.86,
                ],
                3 => [
                    5 => 0,
                    10 => 0,
                    25 => 3,
                    50 => 5,
                    75 => 7,
                    90 => 13.3,
                    95 => 16.3,
                    'moyenne' => 5.33,
                    'ecart_type' => 3.5,
                ],
            ],
        ],

        'perseverations' => [
            '<40' => [
                1 => [
                    5 => 0,
                    10 => 0,
                    25 => 0,
                    50 => 0,
                    75 => 0,
                    90 => 3.7,
                    95 => 5.35,
                    'moyenne' => 0.46,
                    'ecart_type' => 1.5,
                ],
                2 => [
                    5 => 0,
                    10 => 0,
                    25 => 0,
                    50 => 0,
                    75 => 1.5,
                    90 => 3,
                    95 => 6,
                    'moyenne' => 0.89,
                    'ecart_type' => 1.31,
                ],
                3 => [
                    5 => 0,
                    10 => 0,
                    25 => 0,
                    50 => 0,
                    75 => 1,
                    90 => 2,
                    95 => 3,
                    'moyenne' => 0.5,
                    'ecart_type' => 1.07,
                ],
            ],
            '40-59' => [
                1 => [
                    5 => 0,
                    10 => 0,
                    25 => 0,
                    50 => 0,
                    75 => 1.5,
                    90 => 4.8,
                    95 => 6.8,
                    'moyenne' => 0.56,
                    'ecart_type' => 0.96,
                ],
                2 => [
                    5 => 0,
                    10 => 0,
                    25 => 0,
                    50 => 0,
                    75 => 2,
                    90 => 6.4,
                    95 => 10,
                    'moyenne' => 0.76,
                    'ecart_type' => 1.26,
                ],
                3 => [
                    5 => 0,
                    10 => 0,
                    25 => 0,
                    50 => 0,
                    75 => 2,
                    90 => 3.1,
                    95 => 4.55,
                    'moyenne' => 0.73,
                    'ecart_type' => 1.1,
                ],
            ],
            '>=60' => [
                1 => [
                    5 => 0,
                    10 => 0,
                    25 => 0,
                    50 => 2,
                    75 => 3,
                    90 => 8.8,
                    95 => 12,
                    'moyenne' => 2,
                    'ecart_type' => 2.81,
                ],
                2 => [
                    5 => 0,
                    10 => 0,
                    25 => 0,
                    50 => 1,
                    75 => 3,
                    90 => 7,
                    95 => 11,
                    'moyenne' => 1.23,
                    'ecart_type' => 1.72,
                ],
                3 => [
                    5 => 0,
                    10 => 0,
                    25 => 0,
                    50 => 1,
                    75 => 2,
                    90 => 3.3,
                    95 => 7,
                    'moyenne' => 0.98,
                    'ecart_type' => 1.31,
                ],
            ],
        ],

        'seuils' => [
            '20-40' => [
                1 => ['categories' => 4, 'errors' => 16, 'percentiles' => 5],
                2 => ['categories' => 4, 'errors' => 16, 'percentiles' => 5],
                3 => ['categories' => 4, 'errors' => 12, 'percentiles' => 4],
            ],
            '41-60' => [
                1 => ['categories' => 3, 'errors' => 25, 'percentiles' => 9],
                2 => ['categories' => 3, 'errors' => 20, 'percentiles' => 9],
                3 => ['categories' => 4, 'errors' => 15, 'percentiles' => 6],
            ],
            '>60' => [
                1 => ['categories' => 2, 'errors' => 30, 'percentiles' => 12],
                2 => ['categories' => 2, 'errors' => 30, 'percentiles' => 12],
                3 => ['categories' => 3, 'errors' => 17, 'percentiles' => 7],
            ],
        ],

    ];

    private function getRangeAge(int $age, string $case = 'default'): ?string
    {
        switch ($case) {
            case 'default':
                if ($age >= 20 && $age <= 40) {
                    return '20-40';
                }
                if ($age >= 41 && $age <= 60) {
                    return '41-60';
                }
                if ($age > 60) {
                    return '>60';
                }
                break;

            case 'alternative':
                if ($age <= 40) {
                    return '<40';
                }
                if ($age >= 41 && $age < 60) {
                    return '41-59';
                }
                if ($age > 60) {
                    return '>=60';
                }
                break;
        }

        return null;
    }

    public function mount($id, $uuid): void
    {
        $this->uuid = $uuid;
        $this->test = Test::find($id);

        // For demo only
        $this->fake();

    }

    public function fake()
    {
        $criteria = ['color', 'shape', 'number', 'other'];
        $answer = ['+', 'e', 'ep'];

        foreach ($this->form->mcst as $key => $value) {
            $this->form->mcst[$key]['criteria'] = $criteria[array_rand($criteria)];

            if ($this->form->mcst[$key]['criteria'] == 'other') {
                $this->form->mcst[$key]['criteria_reason'] = 'My reason';
            }

            $this->form->mcst[$key]['answer'] = $answer[array_rand($answer)];
        }

        $this->form->time = '00:10:00';

    }

    private function handleData()
    {
        if (empty(session('patient.informations'))) {
            $this->dispatch('notify',
                ['message' => trans('messages.patient_missing_infos'), 'type' => 'warning']
            );

            return;
        }

        // Patient information
        $patient = session('patient.informations');
        $patient_age = Carbon::parse($patient['birthdate'])->age;
        $range_age_default = $this->getRangeAge($patient_age, 'default');
        $range_age_alternative = $this->getRangeAge($patient_age, 'alternative');

        if (! $range_age_default || ! $range_age_alternative) {
            $this->dispatch('notify',
                ['message' => __('messages.test_error_age'), 'type' => 'danger']
            );

            return;
        }

        // Mcst data
        $mcst_comments = collect($this->form->mcst)->where('criteria', 'other');

        $seuils = collect($this->normes['seuils'][$range_age_default][$patient['school_grade']]);

        // Catégories
        $categories = collect($this->normes['categories'][$range_age_alternative][$patient['school_grade']]);

        $this->form->mcst_results['nbr_categories_completed']['deviation'] = round((intval($this->form->mcst_results['nbr_categories_completed']['score']) - $categories['moyenne']) / $categories['ecart_type'],
            2);
        $this->form->mcst_results['nbr_categories_completed']['moyenne'] = $categories['moyenne'];
        $this->form->mcst_results['nbr_categories_completed']['ecart_type'] = $categories['ecart_type'];
        $this->form->mcst_results['nbr_categories_completed']['comment'] = trans_choice('messages.mcst_results_comment', $this->form->mcst_results['nbr_categories_completed']['deviation'], ['value' => $this->form->mcst_results['nbr_categories_completed']['deviation']]);

        // Total errors
        $total_errors = collect($this->normes['total_errors'][$range_age_alternative][$patient['school_grade']]);
        $this->form->mcst_results['nbr_errors_no']['deviation'] = round((intval($this->form->mcst_results['nbr_errors_no']['score']) - $total_errors['moyenne']) / $total_errors['ecart_type'],
            2);
        $this->form->mcst_results['nbr_errors_no']['moyenne'] = $total_errors['moyenne'];
        $this->form->mcst_results['nbr_errors_no']['ecart_type'] = $total_errors['ecart_type'];
        $this->form->mcst_results['nbr_errors_no']['comment'] = trans_choice('messages.mcst_results_comment', $this->form->mcst_results['nbr_errors_no']['deviation'], ['value' => $this->form->mcst_results['nbr_errors_no']['deviation']]);

        // Perseverations
        $perseverations = collect($this->normes['perseverations'][$range_age_alternative][$patient['school_grade']]);
        $this->form->mcst_results['nbr_perseverations']['deviation'] = round((intval($this->form->mcst_results['nbr_perseverations']['score']) - $perseverations['moyenne']) / $perseverations['ecart_type'],
            2);
        $this->form->mcst_results['nbr_perseverations']['moyenne'] = $perseverations['moyenne'];
        $this->form->mcst_results['nbr_perseverations']['ecart_type'] = $perseverations['ecart_type'];
        $this->form->mcst_results['nbr_perseverations']['comment'] = trans_choice('messages.mcst_results_comment', $this->form->mcst_results['nbr_perseverations']['deviation'], ['value' => $this->form->mcst_results['nbr_perseverations']['deviation']]);

        dd($this->form->mcst_results);

    }

    public function save()
    {

        $this->validate();

        $this->handleData();

        // dd($this->form->toArray());

        $data = [
            'uuid' => $this->uuid,
            'name' => $this->name,
            'test' => $this->test->name,
            'data' => rand(1, 100),
            // 'form' => $this->form->toArray(),
            'is_completed' => true,
        ];

        // $this->dispatch('updated-testList', $data);

    }

    public function render()
    {
        return view('livewire.tests-patient.mcst');
    }
}
