<?php

namespace App\Livewire\TestsPatient;

use App\Livewire\Forms\TestsPatient\RlriForm;
use App\Models\Test;
use Livewire\Attributes\Locked;
use Livewire\Component;

class Rlri extends Component
{
    #[Locked]
    public string $uuid;

    #[Locked]
    public string $name = 'rlri';

    public Test $test;

    public RlriForm $form;

    public function mount($id, $uuid): void
    {
        $this->uuid = $uuid;
        $this->test = Test::find($id);
    }

    public function save()
    {

        $test = collect(session('patient.testsList'))
            ->where('uuid', $this->uuid);

        dd($this->form->toArray());

        $this->validate();

        $data = [
            'uuid' => $this->uuid,
            'name' => $this->name,
            'test' => $this->test->name,
            'data' => rand(1, 100),
            // 'form' => $this->form->toArray(),
            'is_completed' => true,
        ];

        $this->dispatch('updated-testList', $data);

    }

    public function render()
    {
        return view('livewire.tests-patient.rlri');
    }
}
