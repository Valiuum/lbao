<?php

namespace App\Livewire\TestsPatientCategories;

use App\Models\Category;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithPagination;

class UserList extends Component
{
    use withPagination;

    public array $selection = [];

    public string $search = '';

    public string $orderField = 'name';

    public string $orderDirection = 'ASC';

    public int $paginate = 10;

    public int $editId = 0;

    protected array $queryString = [
        'search' => ['except' => ''],
        'orderField' => ['except' => 'name'],
        'orderDirection' => ['except' => 'ASC'],
    ];

    public function startEdit(int $id): void
    {
        $this->editId = $id;
    }

    public function updating($field, $value): void
    {
        if ($field === 'search') {
            $this->resetPage();
        }
    }

    public function deleteSelected(): void
    {

        foreach ($this->selection as $id) {
            $category = Category::find($id);
            if ($category) {
                if (Auth::user()->can('delete', $category)) {
                    $category->delete();
                } else {
                    abort(403, __('messages.unauthorized_delete_category'));
                }
            }
        }
        $this->notifyDelete();
    }

    public function notifyDelete(): void
    {
        $this->dispatch('notify',
            ['message' => trans_choice('messages.category_deleted', count($this->selection)), 'type' => 'success']);
        $this->selection = [];
    }

    public function paginationView()
    {
        return 'livewire.components.pagination';
    }

    public function setOrderField(string $field)
    {
        if ($this->orderField === $field) {
            $this->orderDirection = $this->orderDirection === 'ASC' ? 'DESC' : 'ASC';
        } else {
            $this->orderField = $field;
            $this->reset('orderDirection');
        }
    }

    public function render()
    {
        $categories = Category::orderBy($this->orderField, $this->orderDirection)->paginate($this->paginate);

        if (strlen($this->search) > 2) {

            $categories = Category::where('name', 'like', "%$this->search%")
                ->orderBy($this->orderField, $this->orderDirection)
                ->paginate($this->paginate);

        }

        return view('livewire.tests-patient-categories.user-list', compact('categories'));
    }
}
