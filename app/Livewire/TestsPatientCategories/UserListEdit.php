<?php

namespace App\Livewire\TestsPatientCategories;

use App\Livewire\Forms\TestsPatientProcedures\EditCategoryForm;
use App\Models\Category;
use Illuminate\Support\Str;
use Livewire\Component;

class UserListEdit extends Component
{
    public EditCategoryForm $form;

    public Category $category;

    public bool $displayTable = false;

    public bool $edit = false;

    public function mount(bool $displayTable, bool $edit, ?Category $category = null): void
    {

        if ($category) {
            $this->category = $category;
            $this->form->fill($category->getAttributes());
        }

        if ($edit) {
            $this->edit = true;
        }

        $this->displayTable = $displayTable;

    }

    public function save(): void
    {
        $this->form->slug = Str::slug($this->form->name);
        $this->validate();

        if ($this->edit) {
            $this->category->create($this->form->toArray());
            session()->flash('success', trans('messages.category_created'));
        } else {
            $this->category->update($this->form->toArray());
            session()->flash('success', trans('messages.category_modified'));
        }
        $this->redirectIntended('/categories/list');
    }

    public function render()
    {
        return view('livewire.tests-patient-categories.user-list-edit');
    }
}
