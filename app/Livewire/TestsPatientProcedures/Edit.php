<?php

namespace App\Livewire\TestsPatientProcedures;

use App\Livewire\Forms\TestsPatientProcedures\EditProcedureForm;
use App\Models\Category;
use App\Models\Test;
use Illuminate\Support\Str;
use Livewire\Attributes\Locked;
use Livewire\Attributes\On;
use Livewire\Component;

class Edit extends Component
{
    public Test $test;

    #[Locked]
    public array $categories;

    public array $optionsList = [];

    public array $optionsVersions = [];

    public array $optionsUsers = [];

    public EditProcedureForm $form;

    public function mount($test = null)
    {

        if ($test) {
            $this->test = $test;
            $this->form->fill($test->getAttributes());
            $this->optionsList = $test->categories->select(['id', 'name'])->toArray();
            $this->optionsVersions = collect(json_decode($test->versions))->toArray();
            $this->optionsUsers = $test->users->select(['id', 'name'])->toArray();
        } else {
            $this->test = new Test;
            $this->form->fill($this->test->getAttributes());
            $this->form->versions = '[]';
            $this->optionsVersions = [];
        }

        $this->categories = Category::all()->toArray();
    }

    #[On('quill-content-update')]
    public function updatedContent($content)
    {
        $this->form->content = $content;
        $this->save();
    }

    public function save()
    {
        $optionsVersions = '['.implode(',', array_map(fn ($version) => '"'.$version.'"', $this->optionsVersions)).']';
        $this->form->versions = $optionsVersions;
        $this->form->categories = collect($this->optionsList)->pluck('id')->toArray();
        $this->form->users = collect($this->optionsUsers)->pluck('id')->toArray();
        $this->form->slug = Str::slug($this->form->name);

        $this->validate();

        if ($this->form->id === 0) {
            $test = $this->test->create($this->form->toArray());
            $test->users()->sync($this->form->users);
            $test->categories()->sync($this->form->categories);
            session()->flash('success', trans('messages.test_created'));
        } else {
            if ($this->test->users()->exists()) {
                $this->test->users()->sync($this->form->users);
            }
            if ($this->test->categories()->exists()) {
                $this->test->categories()->sync($this->form->categories);
            }
            $this->test->update($this->form->toArray());
            session()->flash('success', trans('messages.test_modified'));
        }
        $this->redirectIntended('/tests/list');
    }

    public function render()
    {
        return view('livewire.tests-patient-procedures.edit');
    }
}
