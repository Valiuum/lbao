<?php

namespace App\Livewire\TestsPatientProcedures;

use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithPagination;

class UserList extends Component
{
    use withPagination;

    protected int $paginate = 8;

    public function paginationView()
    {
        return 'livewire.components.pagination';
    }

    public function render()
    {

        $tests = Auth::user()
            ->tests()
            ->with('categories')
            ->orderBy('name', 'asc')
            ->paginate($this->paginate);

        // Get the collection from the paginator
        $collection = $tests->getCollection();
        $tests->setCollection($collection);

        return view('livewire.tests-patient-procedures.user-list', [
            'tests' => $tests,
        ]);
    }
}
