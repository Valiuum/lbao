<?php

namespace App\Policies;

use App\Models\Test;
use App\Models\User;

class TestPolicy
{
    /**
     * Determine whether the user can view any models.
     */
    public function viewAny(User $user): bool
    {
        return false;
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(User $user, Test $test): bool
    {
        return false;
    }

    /**
     * Determine whether the user can create models.
     */
    public function create(User $user): bool
    {
        $allowed_users = User::all()->where('role', 'admin')->pluck('id')->toArray();

        return in_array($user->id, $allowed_users);
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(User $user, Test $test): bool
    {
        $allowed_users = $test->users()->pluck('user_id')->toArray();

        return in_array($user->id, $allowed_users);
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(User $user, Test $test): bool
    {
        $allowed_users = User::all()->where('role', 'admin')->pluck('id')->toArray();

        // $allowed_users = $test->users()->pluck('user_id')->toArray();
        return in_array($user->id, $allowed_users);
    }

    /**
     * Determine whether the user can restore the model.
     */
    public function restore(User $user, Test $test): bool
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the model.
     */
    public function forceDelete(User $user, Test $test): bool
    {
        return false;
    }
}
