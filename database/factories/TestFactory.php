<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Test>
 */
class TestFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {

        $versions = $this->faker->randomElements([
            'version 1',
            'version 2',
            'version alternative',
        ], $this->faker->numberBetween(1, 3));

        $test_name = $this->faker->words($this->faker->numberBetween(1, 3), true);

        return [
            'name' => $test_name,
            'slug' => Str::slug($test_name),
            'versions' => json_encode($versions),
            'content' => $this->faker->sentence(200),
            'is_active' => $this->faker->boolean,
        ];
    }
}
