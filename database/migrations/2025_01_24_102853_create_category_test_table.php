<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('category_test', function (Blueprint $table) {
            $table->foreignIdFor(\App\Models\Category::class)
                ->constrained()
                ->onDelete('cascade');
            $table->foreignIdFor(\App\Models\Test::class)
                ->constrained()
                ->onDelete('cascade');
            $table->primary(['category_id', 'test_id']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('category_test');
    }
};
