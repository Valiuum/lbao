<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Test;
use App\Models\User;
use Illuminate\Database\Seeder;

class TestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $users = User::all();
        $categories = Category::all();

        for ($i = 0; $i < 19; $i++) {

            $test_create = Test::factory()->create();
            $test = Test::find($test_create->id);

            // Sync with categories
            $test->categories()->attach($categories->random());

            // Sync with users
            $test->users()->attach($users->random());

            $test->save();
        }

        $test_create = Test::factory()->create(
            [
                'name' => 'RLRI',
                'slug' => 'rlri',
                'content' => 'Ceci est mon contenu de test RLRI',
                'versions' => json_encode(['Version RLRI 1', 'Version RLRI 2', 'Version RLRI 3']),
                'is_active' => true,
            ]
        );

        $test = Test::find($test_create->id);
        $test->categories()->attach([
            $categories->random()->id,
            $categories->random()->id,
        ]);
        $test->users()->attach([
            $users->random()->id,
            $users->random()->id,
        ]);
        $test->save();
    }
}
