<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::factory(1)->create([
            'name' => 'Valentin',
            'email' => 'valentindufour@live.fr',
            'password' => Hash::make('fakepassword'),
            'role' => 'admin',
        ]);

        User::factory(1)->create([
            'name' => 'Test User',
            'email' => 'test@test.fr',
            'password' => Hash::make('test'),
        ]);

        User::factory(1)->create([
            'name' => 'Second Test User',
            'email' => 'second_test@test.fr',
            'password' => Hash::make('test'),
        ]);
    }
}
