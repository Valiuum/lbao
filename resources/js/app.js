import './bootstrap'
import 'material-symbols'
import sort from '@alpinejs/sort'
import * as QuillTableUI from 'quill-table-ui'
Quill.register({
    'modules/tableUI': QuillTableUI.default
}, true)

export const events = ['DOMContentLoaded', 'livewire:load', 'livewire:navigated', 'alpine:init'];
export const darkModePreference = window.matchMedia("(prefers-color-scheme: dark)");
const lightModePreference = window.matchMedia("(prefers-color-scheme: light)");

Alpine.plugin(sort)

const activateDarkMode = () => {
    let buttons = document.querySelectorAll('.button')
    let notifications = document.querySelectorAll('.notification')
    let cards = document.querySelectorAll('.card')
    let boxes = document.querySelectorAll('.box')
    let testVisibility = document.querySelectorAll('.test-visibility')
    if (window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) {
        buttons.forEach(button => {
            if (button.classList.contains('is-light')) {
                button.classList.replace('is-light', 'was-light');
            }
            button.classList.add('is-dark');
        })
        notifications.forEach(notification => {
            if (notification.classList.contains('has-background-danger-light')) {
                notification.classList.replace('has-background-danger-light', 'has-background-danger-dark');
            }
            if (notification.classList.contains('has-background-warning-light')) {
                notification.classList.replace('has-background-warning-light', 'has-background-warning-dark');
            }
            if (notification.classList.contains('has-background-info-light')) {
                notification.classList.replace('has-background-info-light', 'has-background-info-dark');
            }
            if (notification.classList.contains('has-background-success-light')) {
                notification.classList.replace('has-background-success-light', 'has-background-success-dark');
            }
        })
        testVisibility.forEach(test => {
            if (test.classList.contains('is-light')) {
                test.classList.replace('is-light', 'is-dark');
            }
        })
        cards.forEach(card => {
          card.classList.add('is-shadowless');
        })
        boxes.forEach(box => {
            box.classList.add('is-shadowless');
        })
        sessionStorage.setItem('darkMode', 'true');
        //window.dispatchEvent(new CustomEvent('color-mode-update', {detail: ['is-dark'], bubbles: true}))
    } else {
        buttons.forEach(button => {
            button.classList.remove('is-dark');
            if (button.classList.contains('was-light')) {
                button.classList.replace('was-light', 'is-light');
            }
        });
        cards.forEach(card => {
            card.classList.remove('is-shadowless');
        })
        boxes.forEach(box => {
            box.classList.remove('is-shadowless');
        })
        sessionStorage.setItem('darkMode', 'false');
        //window.dispatchEvent(new CustomEvent('color-mode-update', {detail: ['is-light'], bubbles: true}))
    }
}

const modalManager = {
    openModal: function ($el) {
        $el.classList.add('is-active');
    },

    closeModal: function ($el) {
        $el.classList.remove('is-active');
    },

    closeAllModals: function () {
        (document.querySelectorAll('.modal') || []).forEach(($modal) => {
            this.closeModal($modal);
        });
    },

    init: function () {
        // Add a click event on buttons to open a specific modal
        (document.querySelectorAll('.js-modal-trigger') || []).forEach(($trigger) => {
            const modal = $trigger.dataset.target;
            const $target = document.getElementById(modal);

            $trigger.addEventListener('click', () => {
                this.openModal($target);
            });
        });

        // Add a click event on various child elements to close the parent modal
        (document.querySelectorAll('.modal-background, .modal-close, .modal-card-head .delete, .modal-card-foot .button') || []).forEach(($close) => {
            const $target = $close.closest('.modal');

            $close.addEventListener('click', () => {
                this.closeModal($target);
            });
        });

        // Add a keyboard event to close all modals
        document.addEventListener('keydown', (event) => {
            if (event.key === "Escape") {
                this.closeAllModals();
            }
        });
    }

}
const handleFunctions = () => {
    modalManager.init()
    activateDarkMode()
}

darkModePreference.addEventListener("change", e => e.matches && activateDarkMode());
lightModePreference.addEventListener("change", e => e.matches && activateDarkMode());

events.forEach(event => {
    document.addEventListener(event, handleFunctions);
});

/*window.addEventListener("beforeunload", function (e) {
    let confirmationMessage = "\\o/";

    (e || window.event).returnValue = confirmationMessage
    return confirmationMessage
});*/

