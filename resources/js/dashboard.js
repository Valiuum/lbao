import {events} from './app.js'

let DASHBOARD_SIDEBAR_LEFT;
let DASHBOARD_SIDEBAR_RIGHT;
let DASHBOARD_SIDEBAR_RIGHT_WIDTH = 'sidebarRightWidth';
let DASHBOARD_SIDEBAR_RIGHT_MIN_WIDTH;
let DASHBOARD_SIDEBAR_RIGHT_DEFAULT_WIDTH;
let TOGGLE_SIDEBAR_BUTTON_RIGHT;
let QUILL_EMPTY;
let QUILL_CONTAINER;
let QUILL_CONTENT;
let PATIENT_TESTS_EMPTY;

function setDomElements() {
    DASHBOARD_SIDEBAR_LEFT = document.getElementById('dashboard-sidebar-left')
    DASHBOARD_SIDEBAR_RIGHT = document.getElementById('dashboard-sidebar-right')
    DASHBOARD_SIDEBAR_RIGHT_MIN_WIDTH = getComputedStyle(document.documentElement).getPropertyValue('--dashboard-sidebar-right-min-width').trim()
    DASHBOARD_SIDEBAR_RIGHT_DEFAULT_WIDTH = getComputedStyle(document.documentElement).getPropertyValue('--dashboard-sidebar-right-width').trim()
    TOGGLE_SIDEBAR_BUTTON_RIGHT = document.getElementById('dashboard-sidebar-right-btn-toggle')
    QUILL_EMPTY = document.getElementsByClassName('quill-empty')[0]
    QUILL_CONTAINER = document.getElementsByClassName('quill-container')[0]
    QUILL_CONTENT = DASHBOARD_SIDEBAR_RIGHT ? DASHBOARD_SIDEBAR_RIGHT.children[1].children[1].children[0] : null
    PATIENT_TESTS_EMPTY = document.getElementsByClassName('patient-tests-is-empty')[0]
}

window.dashboard = () => {

    return {

        toggleAsideButtonLeft: document.getElementById('dashboard-sidebar-left-btn-toggle'),

        search: '',

        toggle(e) {

            const ul = e.parentElement.nextElementSibling;

            if (!ul.classList.contains('show')) {
                this.closeAllSubMenus()
            }

            if (ul) {
                ul.classList.toggle('show');
                e.classList.toggle('rotate')
            }

            if (DASHBOARD_SIDEBAR_LEFT.classList.contains('close')) {
                DASHBOARD_SIDEBAR_LEFT.classList.toggle('close');
                this.toggleAsideButtonLeft.classList.toggle('rotate');
                sessionStorage.setItem('sidebarLeft', 'open')
            }

        },

        show_item(el) {

            const element = el.children[0]
            const searchElement = element.classList.contains('dropdown-menu-element') ? element.children[1] : element.children[0];
            const searchMatch = this.search === '' || searchElement.textContent.toLowerCase().trim().includes(this.search.toLowerCase().trim())
            if (searchMatch) {
                const parentDropdown = el.firstElementChild;
                if (parentDropdown && parentDropdown.classList.contains('dropdown-menu-element') && this.search !== '') {
                    const dropdownElement = parentDropdown.parentElement.parentElement.children[1];
                    if (dropdownElement.classList.contains('sub-menu')) {
                        dropdownElement.classList.add('show');
                        const dropdownDiv = dropdownElement.children[0]
                        Array.of(...dropdownDiv.children).forEach((li) => {
                            li.children[0].style.display = 'block';
                        })
                    }
                } else {
                    const dropdown = el.parentElement.parentElement.parentElement;
                    const dropdownParent = dropdown.previousElementSibling;
                    if (dropdownParent && dropdownParent.classList.contains('icon-text') && this.search !== '') {
                        dropdownParent.style.display = 'block';
                        dropdownParent.children[0].classList.add('rotate')
                        const dropdownDiv = dropdownParent.parentElement.children[1]
                        dropdownDiv.classList.add('show');
                    }
                }

                if (this.search === '') {

                    const dropdownElements = document.getElementsByClassName('sub-menu');

                    Array.from(dropdownElements).forEach((ul) => {

                        if (ul.classList.contains('show')) {
                            ul.classList.remove('show');
                        }

                        const dropdownParent = ul.parentElement;
                        if(dropdownParent.classList.contains('menu-element')) {
                            const dropdownLink = dropdownParent.firstElementChild.firstElementChild
                            if(dropdownLink.classList.contains('rotate')) {
                                dropdownLink.classList.remove('rotate')
                            }
                        }

                    })

                }

            }
            return searchMatch;
        },

        toggleSidebar(sidebar) {

            if (sidebar === 'left') {
                DASHBOARD_SIDEBAR_LEFT.classList.toggle('close')
                this.toggleAsideButtonLeft.classList.toggle('rotate')
                this.closeAllSubMenus()
                sessionStorage.setItem('sidebarLeft', DASHBOARD_SIDEBAR_LEFT.classList.contains('close') ? 'close' : 'open')
            }

            if (sidebar === 'right') {
                TOGGLE_SIDEBAR_BUTTON_RIGHT.classList.toggle('rotate')
                DASHBOARD_SIDEBAR_RIGHT.classList.toggle('close')
                if (TOGGLE_SIDEBAR_BUTTON_RIGHT.classList.contains('rotate')) {
                    sessionStorage.setItem('sidebarRight', DASHBOARD_SIDEBAR_RIGHT.classList.contains('close') ? 'close' : 'open')
                    sessionStorage.setItem(DASHBOARD_SIDEBAR_RIGHT_WIDTH, DASHBOARD_SIDEBAR_RIGHT_MIN_WIDTH)
                    document.body.style.setProperty('--dashboard-sidebar-right-width', DASHBOARD_SIDEBAR_RIGHT_MIN_WIDTH)
                    QUILL_EMPTY.classList.add('is-hidden')
                    QUILL_CONTAINER.classList.add('is-hidden')
                } else {
                    sessionStorage.setItem(DASHBOARD_SIDEBAR_RIGHT_WIDTH, DASHBOARD_SIDEBAR_RIGHT_DEFAULT_WIDTH)
                    document.body.style.setProperty('--dashboard-sidebar-right-width', DASHBOARD_SIDEBAR_RIGHT_DEFAULT_WIDTH)
                    QUILL_EMPTY.classList.remove('is-hidden')
                    QUILL_CONTAINER.classList.add('is-hidden')
                }
            }

        },

        quillSidebarContent(action) {

            const currentWidth = DASHBOARD_SIDEBAR_RIGHT.getBoundingClientRect().width;

            if (action === 'add') {
                const quillContent = QUILL_CONTENT.children[0].innerText.length
                const sidebarRightWidth = sessionStorage.getItem(DASHBOARD_SIDEBAR_RIGHT_WIDTH)

                sessionStorage.setItem('sidebarRight', 'open')
                let sessionStorageWidth = sessionStorage.getItem(DASHBOARD_SIDEBAR_RIGHT_WIDTH)
                if (parseFloat(sessionStorageWidth) <= parseFloat(DASHBOARD_SIDEBAR_RIGHT_MIN_WIDTH))
                {
                    QUILL_EMPTY.classList.add('is-hidden')
                    QUILL_CONTAINER.classList.add('is-hidden')
                } else {
                    if(quillContent === 0) {
                        QUILL_EMPTY.classList.remove('is-hidden')
                        QUILL_CONTAINER.classList.add('is-hidden')
                    } else {
                        QUILL_EMPTY.classList.add('is-hidden')
                        QUILL_CONTAINER.classList.remove('is-hidden')
                    }
                }

            }

            if (action === 'show') {

                TOGGLE_SIDEBAR_BUTTON_RIGHT.classList.remove('rotate')
                DASHBOARD_SIDEBAR_RIGHT.classList.remove('close')
                sessionStorage.setItem('sidebarRight', 'open')
                let width = sessionStorage.getItem(DASHBOARD_SIDEBAR_RIGHT_WIDTH)
                if (parseFloat(width) <= parseFloat(DASHBOARD_SIDEBAR_RIGHT_MIN_WIDTH))
                {
                    width = DASHBOARD_SIDEBAR_RIGHT_DEFAULT_WIDTH
                }
                document.body.style.setProperty('--dashboard-sidebar-right-width', width)
                QUILL_EMPTY.classList.add('is-hidden')
                QUILL_CONTAINER.classList.remove('is-hidden')

            } else if (action === 'hide') {

                if (currentWidth <= parseFloat(DASHBOARD_SIDEBAR_RIGHT_MIN_WIDTH)) {
                    DASHBOARD_SIDEBAR_RIGHT.classList.add('close')
                    document.body.style.setProperty('--dashboard-sidebar-right-width', DASHBOARD_SIDEBAR_RIGHT_MIN_WIDTH)
                    sessionStorage.setItem('sidebarRight', 'close')
                    TOGGLE_SIDEBAR_BUTTON_RIGHT.classList.remove('rotate')
                    QUILL_EMPTY.classList.add('is-hidden')
                    QUILL_CONTAINER.classList.add('is-hidden')
                } else {
                    QUILL_EMPTY.classList.remove('is-hidden')
                    QUILL_CONTAINER.classList.add('is-hidden')
                }

            }

        },

        closeAllSubMenus() {
            Array.from(DASHBOARD_SIDEBAR_LEFT.getElementsByClassName('show')).forEach((ul) => {
                ul.classList.remove('show')
                ul.parentElement.firstElementChild.firstElementChild.classList.remove('rotate')
            })
        },
    }
}

window.mcst = () => {
    return {

        tableAnswer($data) {

            let answer_good = 0
            let answer_e = 0
            let answer_ep = 0

            for (let key in $data.form.mcst) {

                if ($data.form.mcst[key].answer === '+') {
                    answer_good++
                } else if ($data.form.mcst[key].answer === 'e') {
                    answer_e++
                } else if ($data.form.mcst[key].answer === 'ep') {
                    answer_ep++
                }

            }

            $data.form.mcst_results.nbr_errors_no.score = answer_ep + answer_e
            $data.form.mcst_results.nbr_perseverations.score = answer_ep
            $data.form.mcst_results.percentage_perseverations.score = Math.round(((answer_ep / (answer_ep + answer_e)) * 100) * 100) / 100
        }

    }
}

events.forEach(event => {
    if (event === 'livewire:navigated') {
        document.addEventListener(event, () => {
            setDomElements();
            dragSidebar();
        });
    }
});

function dragSidebar() {

    if (current_route !== 'dashboard') {
        return;
    }

    /**
     * Transform the element into a resizeable element
     * @param {HTMLElement} element
     * @param {Function} callback called when the element is resized
     */
    function resizer(element, callback) {

        if (!element) {
            return;
        }

        element.addEventListener('pointerdown', onPointerDown)

        /*
         * @param {PointerEvent} e
         */
        function onPointerDown(e) {
            e.preventDefault()
            document.addEventListener('pointermove', onPointerMove)
            document.addEventListener('pointerup', onPointerUp, {once: true})
        }

        /*
         * @param {PointerEvent} e
         */
        function onPointerMove(e) {
            e.preventDefault()
            callback(window.innerWidth - e.pageX)
        }

        /*
         * @param {PointerEvent} e
         */
        function onPointerUp(e) {
            document.removeEventListener('pointermove', onPointerMove)
        }

    }

    function rafThrottle(callback) {
        let requestId = null

        let lastArgs

        const later = (context) => () => {
            requestId = null
            callback.apply(context, lastArgs)
        }

        const throttled = function (...args) {
            lastArgs = args;
            if (requestId === null) {
                requestId = requestAnimationFrame(later(this))
            }
        }

        throttled.cancel = () => {
            cancelAnimationFrame(requestId)
            requestId = null
        }

        return throttled
    }

    function checkSidebarRightPosition(sidebarRightWidth) {

        if (!DASHBOARD_SIDEBAR_RIGHT) {
            return;
        }

        const quillContent = QUILL_CONTENT ? QUILL_CONTENT.children[0].innerText.length : 0

        if (parseFloat(sidebarRightWidth) <= parseFloat(DASHBOARD_SIDEBAR_RIGHT_MIN_WIDTH)) {
            TOGGLE_SIDEBAR_BUTTON_RIGHT.classList.add('rotate')
            DASHBOARD_SIDEBAR_RIGHT.classList.add('close')
            QUILL_CONTAINER.classList.add('is-hidden')
            QUILL_EMPTY.classList.add('is-hidden')
        } else if (parseFloat(sidebarRightWidth) >= parseFloat(DASHBOARD_SIDEBAR_RIGHT_DEFAULT_WIDTH)) {
            TOGGLE_SIDEBAR_BUTTON_RIGHT.classList.remove('rotate')
            DASHBOARD_SIDEBAR_RIGHT.classList.remove('close')
            if (PATIENT_TESTS_EMPTY) {
                QUILL_EMPTY.classList.remove('is-hidden')
                QUILL_CONTAINER.classList.add('is-hidden')
            } else if (quillContent === 0) {
                QUILL_EMPTY.classList.remove('is-hidden')
                QUILL_CONTAINER.classList.add('is-hidden')
            } else {
                QUILL_EMPTY.classList.add('is-hidden')
                QUILL_CONTAINER.classList.remove('is-hidden')
            }
        } else {
            TOGGLE_SIDEBAR_BUTTON_RIGHT.classList.remove('rotate')
            DASHBOARD_SIDEBAR_RIGHT.classList.remove('close')
            if (quillContent === 0) {
                QUILL_EMPTY.classList.remove('is-hidden')
                QUILL_CONTAINER.classList.add('is-hidden')
            } else {
                QUILL_EMPTY.classList.add('is-hidden')
                QUILL_CONTAINER.classList.remove('is-hidden')
            }
        }
    }

    resizer(document.querySelector('.resizer'), rafThrottle(function (x) {
        const sidebarRightWidth = `${x}px`;
        sessionStorage.setItem(DASHBOARD_SIDEBAR_RIGHT_WIDTH, sidebarRightWidth)
        document.body.style.setProperty('--dashboard-sidebar-right-width', `${sidebarRightWidth}`)
        checkSidebarRightPosition(sidebarRightWidth)
    }))

    const sidebarRightWidth = sessionStorage.getItem(DASHBOARD_SIDEBAR_RIGHT_WIDTH)
    if (sidebarRightWidth) {
        document.body.style.setProperty('--dashboard-sidebar-right-width', sidebarRightWidth)
        checkSidebarRightPosition(sidebarRightWidth)
    } else {
        sessionStorage.setItem(DASHBOARD_SIDEBAR_RIGHT_WIDTH, DASHBOARD_SIDEBAR_RIGHT_MIN_WIDTH)
        document.body.style.setProperty('--dashboard-sidebar-right-width', DASHBOARD_SIDEBAR_RIGHT_MIN_WIDTH)
        checkSidebarRightPosition(DASHBOARD_SIDEBAR_RIGHT_MIN_WIDTH)
    }

}





