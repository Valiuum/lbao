window.Timer = class Timer {

    static timer_text(start, current) {
        let h = "00", m = "00", ms = "00", s = "00";
        const diff = current - start;
        // milliseconds
        if (diff > 10) {
            ms = Math.floor((diff % 1000) / 10);
            ms = Timer.leftPad(ms);
        }
        // seconds
        if (diff > 1000) {
            s = Math.floor(diff / 1000);
            s = s > 60 ? s % 60 : s;
            s = Timer.leftPad(s);
        }
        // minutes
        if (diff > 60000) {
            m = Math.floor(diff / 60000);
            m = m > 60 ? m % 60 : Timer.leftPad(m);
        }
        // hours
        if (diff > 3600000) {
            h = Math.floor(diff / 3600000);
            h = Timer.leftPad(h)
        }
        return {
            'raw': h + ':' + m + ':' + s,
            'formatted':
                `<span class="column is-one-third-desktop is-size-4">${h}</span>
                 <span class="column is-one-third-desktop is-size-4">${m}</span>
                 <span class="column is-one-third-desktop is-size-4">${s}</span>`
        }
    }

    static leftPad(val) {
        return val < 10 ? '0' + String(val) : val;
    }

    static copy_value($event, value) {
        const element = document.getElementById('app');
        const notify = {type: 'success', message: 'Copied to form'};
        element.dispatchEvent(new CustomEvent('notify', {detail: [notify], bubbles: true}))
        const current_test_uuid = $event.target.closest('div.item').getAttribute('id');
        if (current_test_uuid) {
            const current_wire_id = document.getElementById(current_test_uuid).getAttribute('wire:id');
            window.Livewire.find(current_wire_id).set('form.time', value);
        }
    }
}
