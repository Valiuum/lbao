@desktop
@auth
    <figure {{ $attributes->merge(['class' => 'image is-48x48 p-1']) }}>
        <img class="is-rounded" src="{{asset('images/common/logo.svg')}}" alt="logo">
    </figure>
@endauth
@guest
    <figure {{ $attributes->merge(['class' => 'image is-64x64']) }}>
        <img class="is-rounded" src="{{asset('images/common/logo.svg')}}" alt="logo">
    </figure>
@endguest
@elsedesktop
@auth
    <figure {{ $attributes->merge(['class' => 'image is-48x48 mx-auto']) }}>
        <img class="is-rounded" src="{{asset('images/common/logo.svg')}}" alt="logo">
    </figure>
@endauth
@guest
    <figure {{ $attributes->merge(['class' => 'image is-48x48 mx-auto']) }}>
        <img class="is-rounded" src="{{asset('images/common/logo.svg')}}" alt="logo">
    </figure>
@endguest
@enddesktop



