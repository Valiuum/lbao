@props(['icon','text'])
<aside id="dashboard-sidebar-left" class="box" :class="{'close': sessionStorage.sidebarLeft === 'close', 'is-shadowless': sessionStorage.getItem('darkMode') === 'true' }">
    <ul>
        <li>
            <button id="dashboard-sidebar-left-btn-toggle" class="is-flex is-align-items-center"
                    @click="dashboard.toggleSidebar('left')">
                <span class="material-symbols-outlined">keyboard_double_arrow_left</span>
            </button>
        </li>
        <li>
            <div class="field">
                <div id="dashboard-aside-search" class="control has-icons-left mb-4">
                    <x-input-text id="dashboard-aside-search-input" type="text" name="search" placeholder="{{trans('messages.search')}}" x-model="dashboard.search"/>
                    <x-input-icon class="icon is-small is-left" icon="search"/>
                </div>
            </div>
        </li>
        @foreach(\App\Models\Category::with('tests')->get() as $category)

            @if($category->tests->isEmpty())
                @continue
            @endif

            <li class="menu-element">
                <x-link-menu-element
                    icon="category"
                    class="dropdown-menu-element"
                    is_openable="true"
                    link_name="{{ $category->name }}"
                />
                <ul class="sub-menu">
                    <div>
                        @foreach($category->tests as $test)

                            @if($test->is_active === 0)
                                @continue
                            @endif

                            <li class="my-2">
                                <x-link-menu-element
                                    route="dashboard"
                                    link_name="{{ $test->name }}"
                                    link_id="{{ $test->id }}"
                                />
                            </li>
                        @endforeach
                    </div>
                </ul>
            </li>
        @endforeach
</aside>



