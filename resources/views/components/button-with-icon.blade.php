@props(['icon','text', 'class', 'action' => null, 'action_parameters' => null, 'disabled' => 'false'])
<button class="button {{$class}}" :class="{'is-dark': sessionStorage.getItem('darkMode') === 'true' }" @if($action) wire:click="{{$action}}('{{ $action_parameters }}')" @endif " @if($disabled === 'true') disabled @endif>
    <span class="material-symbols-outlined @desktop {{$text ? 'mr-2' : ''}} @enddesktop">
    {{ $icon }}
    </span>
    <span class="@tablet is-hidden @endtablet">
        {{ $text }}
    </span>
</button>



