<button :class="{'is-dark': sessionStorage.getItem('darkMode') === 'true' }" {{ $attributes->merge(['type' => 'submit', 'class' => 'button is-danger is-light']) }}>
    {{ $slot }}
</button>
