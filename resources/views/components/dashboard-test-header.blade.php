@props(['name', 'uuid', 'id', 'form'])
<div class="is-flex columns is-align-content-center my-0">
    <div class="is-flex column is-three-fifths is-align-items-center">
        <h6 class="is-size-6 ml-2 tag" :class="{'is-dark': sessionStorage.getItem('darkMode') === 'true' }">{{ucfirst($name)}}</h6>
    </div>
    <div class="is-flex column is-two-fifths is-justify-content-end">
        <div class="field is-grouped">
            <p class="control">
                <button {{$attributes->merge(['class' => 'button is-link is-light test-action toggle is-flex is-align-content-center'])}} :class="{ 'rotate': ! expanded, 'is-dark': sessionStorage.getItem('darkMode') === 'true' }" @click="expanded = ! expanded">
                    <span class="material-symbols-outlined">
                        keyboard_double_arrow_down
                    </span>
                </button>
            </p>
            <p class="control">
                <button {{$attributes->merge(['class' => 'button is-link is-light test-action toggle is-flex is-align-content-center'])}} :class="{'is-dark': sessionStorage.getItem('darkMode') === 'true' }" wire:click="$dispatchTo('editor.quill', 'load-item-description', { data : { id: '{{ $id }}' }})">
                    <span class="material-symbols-outlined">
                        description
                    </span>
                </button>
            </p>
            <p class="control">
                <button {{$attributes->merge(['class' => 'button is-link is-light test-action toggle is-flex is-align-content-center'])}} :class="{'is-dark': sessionStorage.getItem('darkMode') === 'true' }" wire:click="$parent.loadNewItem('{{$id}}')">
                    <span class="material-symbols-outlined">
                        content_copy
                    </span>
                </button>
            </p>
            <p class="control">
                <button {{$attributes->merge(['class' => 'button is-danger is-light test-action toggle is-flex is-align-content-center'])}} :class="{'is-dark': sessionStorage.getItem('darkMode') === 'true' }" wire:click="$parent.removeItem('{{$uuid}}')">
                    <span class="material-symbols-outlined">
                        delete
                    </span>
                </button>
            </p>
        </div>
    </div>
</div>
