@props([
    'id' => uniqid(),
    'optionsValues' => [],
    'optionsList' => [],
    'label' => '',
    'model' => '',
    'disabled' => false,
])

{{--<div wire:key="{{ $id }}" x-data="tagSelect(@entangle($model), {{json_encode($optionsValues)}}, {{$disabled}})">
    <!-- Tag Input -->
    <div class="field">
        <x-input-label for="{{ 'input' . $id }}" :value="$label"/>
        <input x-model="newTag" class="input"
               @keydown.enter.prevent="addTag(newTag.trim());newTag = '';"
               type="text" placeholder="{{ __('messages.test_version_placeholder') }}"
        />
        <!-- Tags will be added here -->
        <div class="tags are-medium mt-3">
            <template x-for="(tag, index) in tags" :key="index">
                <span class="tag is-hoverable is-light">
                    <span x-text="tag"></span>
                    <button
                        @click="tags.splice(index, 1)"
                        class="delete ml-1">
                    </button>
                </span>
            </template>
        </div>
    </div>
</div>--}}

<div x-cloak wire:key="{{ $id }}" class="msa-wrapper {{$disabled ? 'disabled' : ''}}"
     x-data="tagSelect(@entangle($model), {{json_encode($optionsValues)}}, {{$disabled}})">
    <x-input-label for="{{ 'input' . $id }}" :value="$label"/>
    <input
        type="text" id="{{ 'input' . $id }}"
        aria-hidden="true"
        x-bind:aria-expanded="listActive.toString()"
        aria-haspopup="{{$id}}"
        hidden>
    <div class="input-presentation" x-bind:class="{'active': listActive}" @click="listActive = !listActive">
        <div class="tags are-medium">
            <template x-if="tags.length === 0">
                <span class="has-text-grey-light">{{ __('messages.test_version_placeholder') }}</span>
            </template>
            <template x-for="(tag, index) in tags" :key="index">
            <span class="tag is-hoverable">
                <span x-text="tag"></span>
                <button @click.stop="removeTag(index)"
                        class="delete ml-1">
                </button>
            </span>
            </template>
        </div>
    </div>
    <ul id="{{$id}}" class="mt-0 add-test-version multi-select" x-show.transition="listActive" x-transition:enter.duration.200ms x-transition:leave.duration.150ms role="listbox" @click.away="listActive = false">
        <p class="field m-3">
            <input x-model="newTag" class="input"
                   @keydown.enter.prevent="addTag(newTag.trim());newTag = '';"
                   type="text"
            />
        </p>
    </ul>
</div>

@once
    <script>
        function tagSelect(model, optionsValues, disabled) {
            return {
                listActive: false,
                newTag: '',
                tagsList: model,
                tags: optionsValues ? optionsValues : [],
                addTag(tag) {
                    console.log(tag)
                    if (disabled) {
                        return;
                    }
                    if (tag.trim() !== '') {
                        this.tags.push(tag.trim())
                        this.tagsList.push(tag.trim())
                    }
                },
                removeTag(index) {
                    if (disabled) {
                        return;
                    }
                    this.tags.splice(index, 1)
                    this.tagsList.splice(index, 1)
                },
            }
        }
    </script>
@endonce
