@props(['messages'])

@if ($messages)
    <ul {{ $attributes->merge(['class' => 'mt-2 ml-0']) }}>
        @foreach ((array) $messages as $message)
            <p class="help is-danger">{{ $message }}</p>
        @endforeach
    </ul>
@endif
