@props(['value', 'for' => '', 'size' => 'is-normal'])
<div class="field-label {{$size}} py-0">
    <label for="{{$for}}" {{ $attributes->merge(['class' => 'label is-pulled-left m-1']) }}>
        {{ $value ?? $slot }}
    </label>
</div>
