@props([
    'id' => uniqid(),
    'optionsValues' => [],
    'optionsList' => [],
    'model' => '',
    'label' => '',
    'placeholder' => '',
    'disabled' => false,
])

@dump($optionsList)

<div x-cloak wire:key="{{ $id }}" class="msa-wrapper {{$disabled ? 'disabled' : ''}}"
     x-data="multiselectComponent(@entangle($model), {{json_encode($optionsList)}}, {{json_encode($optionsValues)}}, {{$disabled}})">
    <x-input-label for="{{ 'input' . $id }}" :value="$label"/>
    <input
        type="text" id="{{ 'input' . $id }}"
        aria-hidden="true"
        x-bind:aria-expanded="listActive.toString()"
        aria-haspopup="{{$id}}"
        hidden>
    <div class="input-presentation collapsible" @click="listActive = !listActive" @click.away="listActive = false"
         x-bind:class="{'active': listActive}">
        <span class="placeholder" x-show="selected.length == 0">
            <p class="has-text-grey-light">{{$placeholder}}</p>
        </span>
        <div class="tags are-medium">
            <template x-for="(tag, index) in selected" :key="tag.id">
                <span class="tag is-hoverable" x-bind:value="tag.id">
                    <span x-text="tag.name"></span>
                    <button class="delete" x-bind:data-index="index" @click.stop="removeMe($event)"></button>
                </span>
            </template>
        </div>
    </div>
    <ul id="{{$id}}" class="multi-select" x-show.transition="listActive" x-transition:enter.duration.200ms x-transition:leave.duration.150ms role="listbox">
        <template x-for="(tag, index) in unselected" :key="tag.id">
            <li x-show="!selected.includes(tag)"
                x-bind:value="tag.id"
                x-text="tag.name"
                aria-role="button"
                @click.stop="addMe($event)"
                x-bind:data-index="index"
                role="option"
            ></li>
        </template>
    </ul>
</div>

@once
    <script>
        function multiselectComponent(optionsList, optionsSelected, optionsValues, isDisabled) {
            return {
                listActive: false,
                optionsList: optionsList,
                selected: optionsSelected.map(option => ({id: option.id, name: option.name})),
                unselected: optionsValues.filter(option => !optionsSelected.some(selected => selected.id === option.id)).map(option => ({
                    id: option.id,
                    name: option.name
                })),
                addMe(e) {
                    if (isDisabled) return;
                    const index = e.target.dataset.index
                    const extracted = this.unselected.splice(index, 1)
                    this.selected.push(extracted[0])
                    if (!this.optionsList.some(option => option.id === extracted[0].id)) this.optionsList.push(extracted[0])
                },
                removeMe(e) {
                    if (isDisabled) return;
                    const index = e.target.dataset.index
                    const extracted = this.selected.splice(index, 1)
                    this.unselected.push(extracted[0])
                    console.log(extracted[0])
                    const valueIndex = this.optionsList.findIndex(option => option.id === extracted[0].id)
                    if (valueIndex > -1) {
                        this.optionsList.splice(valueIndex, 1)
                    }
                },
            }
        }
    </script>
@endonce


{{--
<div x-cloak x-init="dropdown()">
    <x-input-label>{{$label}}</x-input-label>
    <p>{{$label}}</p>
    <select id="{{ $id }}" multiple data-placeholder="{{$label}}">
        @foreach($optionsValues as $option)
            <option value="{{$option['id']}}"
                    @if(in_array($option['id'], $optionsList->pluck('id')->toArray())) selected @endif>
                {{$option['name']}}
            </option>
        @endforeach
    </select>
    <script>

        function dropdown() {
            return {
                options: [],
                selected: [],
                show: false,
                open() {
                    this.show = true;
                },
                close() {
                    this.show = false;
                },
                isOpen() {
                    return this.show === true;
                },
                select(index, event) {
                    if (!this.options[index].selected) {
                        this.options[index].selected = true;
                        this.selected.push(this.options[index]);
                        this.optionsCategoriesSelected.push(this.options[index].value);
                    }
                },
                remove(index, option) {
                    this.options[option].selected = false;
                    this.selected.splice(index, 1);
                    const valueIndex = this.optionsCategoriesSelected.indexOf(this.options[option].value);
                    if (valueIndex > -1) {
                        this.optionsCategoriesSelected.splice(valueIndex, 1);
                    }
                },
                loadOptions() {
                    const options = @json($optionsValues);
                    const selected = @json($optionsList->pluck('id')->toArray());

                    for (let i = 0; i < options.length; i++) {
                        this.options.push({
                            value: options[i].id,
                            text: options[i].name,
                            selected: selected.includes(options[i].id)
                        });
                    }

                    this.options.forEach((option, index) => {
                        if (option.selected) this.selected.push(option);
                    });
                },
                selectedValues() {
                    return this.selected.map(option => option.value);
                },
            }
        }

        const animationDuration = 300
        const select = document.getElementById('{{ $id }}')
        const selectOptions = select.querySelectorAll('option')
        const isDisabled = {{ $disabled ? 'true' : 'false' }};
        const newSelect = document.createElement('div')
        newSelect.classList.add('selectMultiple')
        const active = document.createElement('div')
        if (isDisabled) {
            active.classList.add('disabled')
        } else {
            active.classList.add('active')
        }
        const optionList = document.createElement('ul')
        const placeholder = select.dataset.placeholder
        const span = document.createElement('span')
        span.innerText = placeholder
        active.appendChild(span)

        selectOptions.forEach((option) => {
            let text = option.innerText
            if (option.selected) {
                let tag = document.createElement('a');
                tag.dataset.value = option.value
                tag.innerHTML = "<em>" + text + "</em><i></i>"
                active.appendChild(tag)
                span.classList.add('hide')
            } else {
                let item = document.createElement('li')
                item.dataset.value = option.value
                item.innerHTML = text
                optionList.appendChild(item)
            }
        });

        const arrow = document.createElement('div')
        arrow.classList.add('arrow')
        active.appendChild(arrow)
        newSelect.appendChild(active)
        newSelect.appendChild(optionList)
        select.parentElement.append(newSelect)
        span.appendChild(select)

        document.querySelector('.selectMultiple ul').addEventListener('click', (e) => {
            let li = e.target.closest('li')
            if (!li) {
                return;
            }
            let select = li.closest('.selectMultiple')
            if (!select.classList.contains('clicked')) {
                select.classList.add('clicked')
                if (li.previousElementSibling) {
                    li.previousElementSibling.classList.add('beforeRemove');
                }
                if (li.nextElementSibling) {
                    li.nextElementSibling.classList.add('afterRemove');
                }
                li.classList.add('remove')
                let a = document.createElement('a')
                a.dataset.value = li.dataset.value
                a.innerHTML = "<em>" + li.innerText + "</em><i></i>"
                a.classList.add('notShown')
                // a.style.display = "none";
                select.querySelector('div').appendChild(a) //might have to check later
                let selectEl = select.querySelector('select')
                let opt = selectEl.querySelector('option[value="' + li.dataset.value + '"]')
                opt.setAttribute('selected', 'selected')
                setTimeout(() => {
                    a.classList.add('shown')
                    select.querySelector('span').classList.add('hide')
                }, animationDuration)

                setTimeout(() => {
                    let styles = window.getComputedStyle(li)
                    let liHeight = styles.height
                    let liPadding = styles.padding
                    let removing = li.animate([
                        {
                            height: liHeight,
                            padding: liPadding
                        },
                        {
                            height: '0px',
                            padding: '0px'
                        }
                    ], {
                        duration: animationDuration, easing: 'ease-in-out'
                    });
                    removing.onfinish = () => {
                        if (li.previousElementSibling) {
                            li.previousElementSibling.classList.remove('beforeRemove')
                        }
                        if (li.nextElementSibling) {
                            li.nextElementSibling.classList.remove('afterRemove')
                        }
                        li.remove();
                        select.classList.remove('clicked')
                    }
                }, animationDuration)

            }
        });

        document.querySelector('.selectMultiple > div').addEventListener('click', (e) => {
            let a = e.target.closest('a')
            let select = e.target.closest('.selectMultiple')
            if (!a) {
                return;
            }
            a.className = ''
            a.classList.add('remove')
            select.classList.add('open')
            let selectEl = select.querySelector('select')
            let opt = selectEl.querySelector('option[value="' + a.dataset.value + '"]')
            opt.removeAttribute('selected')
            a.classList.add('disappear')
            setTimeout(() => {
                // start animation
                let styles = window.getComputedStyle(a)
                let padding = styles.padding
                let deltaWidth = styles.width
                let deltaHeight = styles.height

                let removeOption = a.animate([
                    {
                        width: deltaWidth,
                        height: deltaHeight,
                        padding: padding
                    },
                    {
                        width: '0px',
                        height: '0px',
                        padding: '0px'
                    }
                ], {
                    duration: 0,
                    easing: 'ease-in-out'
                });

                let li = document.createElement('li')
                li.dataset.value = a.dataset.value
                li.innerText = a.querySelector('em').innerText
                li.classList.add('show')
                select.querySelector('ul').appendChild(li)
                setTimeout(() => {
                    if (!selectEl.selectedOptions.length) {
                        select.querySelector('span').classList.remove('hide')
                    }
                    li.className = ''
                }, animationDuration + 50)

                removeOption.onfinish = () => {
                    a.remove();
                }

            }, animationDuration)

        });
        document.querySelectorAll('.selectMultiple > div .arrow, .selectMultiple > div span').forEach((el) => {
            el.addEventListener('click', (e) => {
                if (isDisabled) {
                    return;
                }
                el.closest('.selectMultiple').classList.toggle('open')
            })
        });

    </script>
</div>
--}}
