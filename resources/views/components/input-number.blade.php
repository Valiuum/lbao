@props(['maxValue' => 0])
<div class="is-flex">
    <p class="control mr-1">
    <span type="button" x-bind:class="{'is-dark': items.darkMode}"
          class="button is-link is-outlined decrement-button"
          @click="items.updateBonusPoints('decrement')">
        <span class="material-symbols-outlined">remove</span>
    </span>
    </p>
    <p class="control">
        <input type="text" name="user_bonus" class="input is-link quantity-input has-text-centered" readonly
               x-model="items.userBonus.initial"/>
    </p>
    <p class="control ml-1">
    <span type="button" x-bind:class="{'is-dark': items.darkMode}"
          class="button is-link is-outlined decrement-button"
          @click="items.updateBonusPoints('increment')">
        <span class="material-symbols-outlined">add</span>
    </span>
    </p>
    @error('form.user_bonus')
    <x-input-error :messages="$message"/>
    @enderror
</div>
