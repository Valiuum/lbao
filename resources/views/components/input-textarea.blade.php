@props(['disabled' => false, 'placeholder' => ''])
<textarea {!! $attributes->merge(['class' => 'textarea']) !!} placeholder="{{$placeholder}}"></textarea>
