@props([
    'route' => null,
    'link_name' => null,
    'link_id' => null,
    'icon' => null,
    'is_openable' => null,
    'is_active' => false,
    'is_desktop' => true,
    'class' => ''
    ])
@if($is_openable)
    <span class="icon-text" x-show="dashboard.show_item($el)">
        <a @class(['is-flex', $class]) @click="dashboard.toggle($el)">
            <span class="material-symbols-outlined">
                {{ $icon }}
            </span>
            <span class='ml-2 is-hidden-mobile'>
                {{ $link_name }}
            </span>
            <span class="material-symbols-outlined">keyboard_arrow_down</span>
        </a>
    </span>
@else
    <span x-show="dashboard.show_item($el)">
        <a class="is-flex {{$class}}" @click="dashboard.search = ''" wire:click="loadNewItem({{ $link_id }})">
            @if(!$is_desktop)
                <span class='ml-2'>{{ $link_name }}</span>
            @else
                <span class='ml-2 is-hidden-mobile'>
                    {{ $link_name }}
                </span>
            @endif
        </a>
    </span>
@endif



