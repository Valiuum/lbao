@php
    $timeoutDuration = 3000; // Timeout duration in milliseconds
@endphp

{{-- Live Notifications --}}

<div
    x-data="{
        notifications: [],
        notification_show_count: 0,
        notifyCalled: false,
        timeoutDuration: @json($timeoutDuration),
        remove(mid) {
            this.notification_show_count--;
            let m = this.notifications.filter((m) => { return m.id == mid })
            if (m.length) {
                setTimeout(() => {
                    if(this.notification_show_count===0) {
                        this.notifications = [];
                        this.notifyCalled = false;
                    }
                }, 2000)
            }
            $dispatch('close-me', {id: mid})
        },
    }"
    @notify.window="notifyCalled = true;let mid = Date.now();notifications.push({id: mid, msg: $event.detail[0].message, type: $event.detail[0].type});notification_show_count++; setTimeout(() => { remove(mid) }, timeoutDuration)"
    x-init="$watch('notifications', value => { if (value.length === 0) { notifyCalled = false } })"
    :class="{ 'is-hidden': !notifyCalled }"
    id="notifications-container-live"
>
    <template x-for="(notification, notificationIndex) in notifications" :key="notificationIndex">
        <div
            x-data="{ id: notification.id, show: false }"
            x-init="$nextTick(() => { show = true })"
            x-show="show"
            x-transition:enter="animate__fadeInRight"
            x-transition:leave="animate__fadeOutUp"
            @close-me.window="if ($event.detail.id == id) {show=false}"
            :class="`notification ${notification.type === 'success' ? 'is-primary is-light' : notification.type === 'danger' ?
            'is-danger is-light' : notification.type === 'warning' ? 'is-warning is-light' : notification.type === 'info' ?
            'is-info is-light' : ''} my-2 p-2 ${sessionStorage.darkMode === 'true' ? 'is-dark' : ''}`"
            x-cloak
        >
            <div class="rounded-lg shadow-lg overflow-hidden">
                <div class="p-4">
                    <div class="is-flex items-start mx-3">
                        <span
                            x-bind:class="{
                                'has-text-primary-40 material-symbols-outlined is-flex is-align-self-center mr-2': sessionStorage.darkMode === 'true' && notification.type === 'success',
                                'has-text-primary-20 material-symbols-outlined is-flex is-align-self-center mr-2': sessionStorage.darkMode !== 'true' && notification.type === 'success',
                                'has-text-danger-40 material-symbols-outlined is-flex is-align-self-center mr-2': sessionStorage.darkMode === 'true' && notification.type === 'danger',
                                'has-text-danger-20 material-symbols-outlined is-flex is-align-self-center mr-2': sessionStorage.darkMode !== 'true' && notification.type === 'danger',
                                'has-text-warning-40 material-symbols-outlined is-flex is-align-self-center mr-2': sessionStorage.darkMode === 'true' && notification.type === 'warning',
                                'has-text-warning-20 material-symbols-outlined is-flex is-align-self-center mr-2': sessionStorage.darkMode !== 'true' && notification.type === 'warning',
                                'has-text-info-40 material-symbols-outlined is-flex is-align-self-center mr-2': sessionStorage.darkMode === 'true' && notification.type === 'info',
                                'has-text-info-20 material-symbols-outlined is-flex is-align-self-center mr-2': sessionStorage.darkMode !== 'true' && notification.type === 'info'
                            }"
                            x-text="{
                                'success': 'check_circle',
                                'danger': 'error',
                                'warning': 'warning',
                                'info': 'info'
                            }[notification.type]"
                        ></span>
                        <p x-text="notification.msg" class="mx-3"></p>
                        <button @click="remove(notification.id)" class="delete"></button>
                    </div>
                </div>
            </div>
        </div>
    </template>
</div>

{{-- Session Notifications --}}

<div id="notifications-container-session">
    @session("success")
    <div
        x-data="{showNotification: true, hideNotification: false, timeoutDuration: @json($timeoutDuration)}"
        x-effect="setTimeout(() => { hideNotification = true;}, timeoutDuration)"
        x-show="showNotification"
        x-cloak
        :class="{'animate__fadeInRight': showNotification, 'animate__fadeOutUp': hideNotification, 'is-dark': sessionStorage.darkMode === 'true'}"
        class="notification is-primary is-light {{ session()->has('success') ? '' : 'is-hidden' }}">
        <button @click="hideNotification = true" class="delete"></button>
        <p class="is-flex">
            <span
                x-bind:class="{
                    'has-text-primary-40': sessionStorage.darkMode === 'true',
                    'has-text-primary-20': sessionStorage.darkMode !== 'true',
                    'material-symbols-outlined is-flex is-align-self-center mr-2': true
                }"
                class="material-symbols-outlined is-flex is-align-self-center mr-2">check_circle
            </span>
            {{session("success")}}
        </p>
    </div>
    @endsession

    @session("error")
    <div
        x-data="{showNotification: true, hideNotification: false, timeoutDuration: @json($timeoutDuration)}"
        x-effect="setTimeout(() => { hideNotification = true;}, timeoutDuration)"
        x-show="showNotification"
        x-cloak
        :class="{'animate__fadeInRight': showNotification, 'animate__fadeOutUp': hideNotification, 'is-dark': sessionStorage.darkMode === 'true'}"
        class="notification is-danger is-light {{ session()->has('error') ? '' : 'is-hidden' }}">
        <button @click="hideNotification = true" class="delete"></button>
        <p class="is-flex">
            <span
                x-bind:class="{
                    'has-text-danger-40': sessionStorage.darkMode === 'true',
                    'has-text-danger-20': sessionStorage.darkMode !== 'true',
                    'material-symbols-outlined is-flex is-align-self-center mr-2': true
                }"
                class="material-symbols-outlined is-flex is-align-self-center mr-2">error
            </span>
            {{session("error")}}
        </p>
    </div>
    @endsession

    @session("warning")
    <div
        x-data="{showNotification: true, hideNotification: false, timeoutDuration: @json($timeoutDuration)}"
        x-effect="setTimeout(() => { hideNotification = true; }, timeoutDuration)"
        x-show="showNotification"
        x-cloak
        :class="{'animate__fadeInRight': showNotification, 'animate__fadeOutUp': hideNotification, 'is-dark': sessionStorage.darkMode === 'true'}"
        class="notification is-warning is-light {{ session()->has('warning') ? '' : 'is-hidden' }}">
        <button @click="hideNotification = true" class="delete"></button>
        <p class="is-flex">
            <span
                x-bind:class="{
                    'has-text-warning-40': sessionStorage.darkMode === 'true',
                    'has-text-warning-20': sessionStorage.darkMode !== 'true',
                    'material-symbols-outlined is-flex is-align-self-center mr-2': true
                }"
                class="material-symbols-outlined is-flex is-align-self-center mr-2">warning
            </span>
            {{session("warning")}}
        </p>
    </div>
    @endsession

    @session("info")
    <div
        x-data="{showNotification: true, hideNotification: false, timeoutDuration: @json($timeoutDuration), finalClass: false}"
        x-effect="setTimeout(() => { hideNotification = true; setTimeout(() => { finalClass = true }, 500) }, timeoutDuration)"
        x-show="showNotification"
        x-cloak
        :class="{'animate__fadeInRight': showNotification, 'animate__fadeOutUp': hideNotification, 'is-dark': sessionStorage.darkMode === 'true'}"
        class="notification is-info is-light {{ session()->has('info') ? '' : 'is-hidden' }}">
        <button @click="hideNotification = true, finalClass = true" class="delete"></button>
        <p class="is-flex">
            <span
                x-bind:class="{
                    'has-text-info-40': sessionStorage.darkMode === 'true',
                    'has-text-info-20': sessionStorage.darkMode !== 'true',
                    'material-symbols-outlined is-flex is-align-self-center mr-2': true
                }"
                class="material-symbols-outlined is-flex is-align-self-center mr-2">info
            </span>
            {{session("info")}}
        </p>
    </div>
    @endsession

    @if ($errors->any())
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>Please check the form below for errors</strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
</div>

