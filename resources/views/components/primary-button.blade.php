<button {{ $attributes->merge(['type' => 'submit', 'class' => 'button is-link']) }}  x-bind:class="{'is-dark': sessionStorage.getItem('darkMode') === 'true' }">
    {{ $slot }}
</button>
