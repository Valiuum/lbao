<button {{ $attributes->merge(['type' => 'button', 'class' => 'button is-info']) }}>
    {{ $slot }}
</button>
