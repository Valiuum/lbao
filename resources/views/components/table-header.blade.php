<th class="is-clickable" wire:click="setOrderField('{{$name}}')">
    {{ $slot }}
    @if($visible)
        @if($direction === 'ASC')
            <span class="material-symbols-outlined">
                expand_less
            </span>
        @else
            <span class="material-symbols-outlined">
                expand_more
            </span>
        @endif
    @endif
</th>
