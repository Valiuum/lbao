<div
    class="box timer_container"
    x-cloak
    x-data="{
        interval: 0,
        now: 0,
        start: 0,
        stop: 0
	}">

    <!-- Timer display -->
    <div class="columns column is-four-fifths-desktop is-full-touch mx-auto has-text-centered timer_result"
         x-html="Timer.timer_text(start, now || stop).formatted"
    ></div>
    <div class="columns column is-four-fifths-desktop is-full-touch mx-auto has-text-centered timer_result">
        <div class="column is-one-third-desktop">
            <span class="tag">{{__('messages.timer_hours')}}</span>
        </div>
        <div class="column is-one-third-desktop">
            <span class="tag">{{__('messages.timer_minutes')}}</span>
        </div>
        <div class="column is-one-third-desktop">
            <span class="tag">{{__('messages.timer_seconds')}}</span>
        </div>
    </div>

    <!-- Control buttons -->
    <div class="column is-three-fifths mx-auto is-flex is-justify-content-center">
        <div class="field is-grouped">
            <span
                x-show="!interval && !stop"
                class="button is-link"
                :class="{'is-dark': sessionStorage.getItem('darkMode') === 'true'}"
                @click="start = Date.now();stop = 0;interval = setInterval(() => { now = Date.now() }, 10)">
                {{ __('messages.timer_start') }}
            </span>
            <span x-show="interval"
                  class="button is-link"
                  :class="{'is-dark': sessionStorage.getItem('darkMode') === 'true', 'is-light': sessionStorage.getItem('darkMode') === 'false' }"
                  @click="clearInterval(interval);stop = Date.now();interval = 0;now = 0;">
                {{ __('messages.timer_stop') }}
            </span>
            <span x-show="stop"
                  class="button is-link"
                  :class="{'is-dark': sessionStorage.getItem('darkMode') === 'true', 'is-light': sessionStorage.getItem('darkMode') === 'false' }"
                  @click="start = Date.now() - (stop - start);stop = 0;interval = setInterval(() => { now = Date.now() }, 10)">
                {{ __('messages.timer_resume') }}
            </span>
            <span x-show="stop"
                  class="button is-link"
                  :class="{'is-dark': sessionStorage.getItem('darkMode') === 'true', 'is-light': sessionStorage.getItem('darkMode') === 'false' }"
                  @click="Timer.copy_value($event, Timer.timer_text(start, stop).raw)">
                {{ __('messages.timer_copy') }}
            </span>
            <span x-show="stop" class="button is-danger"
                  :class="{'is-dark': sessionStorage.getItem('darkMode') === 'true', 'is-light': sessionStorage.getItem('darkMode') === 'false' }"
                  @click="start=0;stop=0;">
                {{ __('messages.timer_reset') }}
            </span>
        </div>
    </div>
</div>
