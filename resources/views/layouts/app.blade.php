<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=alexandria:200,400,600,800|fira-code:500&display=swap" rel="stylesheet"/>
    <!-- Favicons -->
    <link rel="shortcut apple-touch-icon" sizes="180x180" href="{{asset('apple-touch-icon.png')}}">
    <link rel="shortcut icon" type="image/png" sizes="32x32" href="{{asset('favicon-32x32.png')}}">
    <link rel="shortcut icon" type="image/png" sizes="16x16" href="{{asset('favicon-16x16.png')}}">
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
    <link rel="manifest" href="{{asset('site.webmanifest')}}">

    <!-- Quill -->
    <script src="https://cdn.jsdelivr.net/npm/quill@2.0.3/dist/quill.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/quill@2.0.3/dist/quill.snow.css" rel="stylesheet">

    <!-- Scripts -->
    @vite(['resources/css/app.scss', 'resources/js/app.js'])
    @if(Str::startsWith(Route::currentRouteName(), 'dashboard'))
        @vite('resources/js/dashboard.js')
        @vite('resources/js/timer.js')
    @endif
    <style>
        [x-cloak] {
            display: none !important;
        }
    </style>
</head>
<body class="{{ Route::currentRouteName() === 'dashboard' ?'dashboard' : ''}}">
<x-notifications/>
@livewire('layout.navigation')
<main id="app" class="mx-0 is-fullheight">
    <section class="columns mx-0">
        <!-- Page Content -->
        <div id="content"
             class="container-fluid column is-12-desktop is-12-touch {{ str_replace('.', '-', Route::currentRouteName()) }} @desktop is-desktop px-5 @elsedesktop is-tablet @enddesktop">

            <div class="page-content"
                 x-data="{{ Route::currentRouteName() === 'dashboard' ? '{dashboard: dashboard()}' : '' }}">
                {{ $slot }}
            </div>
        </div>
    </section>
</main>
<script>
    window.current_route = '{{ Route::currentRouteName() }}';
</script>
</body>
</html>
