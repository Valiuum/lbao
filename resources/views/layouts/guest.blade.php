<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <meta name="mobile-web-app-capable" content="yes">
    <meta name="description" content="La boite à outils">

    <!-- Favicons -->
    <link rel="shortcut apple-touch-icon" sizes="180x180" href="{{asset('apple-touch-icon.png')}}">
    <link rel="shortcut icon" type="image/png" sizes="32x32" href="{{asset('favicon-32x32.png')}}">
    <link rel="shortcut shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
    <link rel="manifest" href="{{asset('site.webmanifest')}}">
    <link rel="mask-icon" href="{{asset('safari-pinned-tab.svg')}}" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=alexandria:200,400,600,800&display=swap" rel="stylesheet" />

    <!-- Scripts -->
    @vite(['resources/css/app.scss', 'resources/js/guest.js'])

</head>

<body class="guest">
<div class="gradient"></div>
<section class="hero is-fullheight">
    <div class="hero-body m-auto p-3">
        <div class="columns mx-auto is-centered">
            <div class="column is-12-touch is-8-tablet is-10-desktop is-centered">
                <div class="card">
                    <div class="card-header is-flex is-justify-content-center is-align-items-center py-3">
                        <a href="/" wire:navigate class="mr-2">
                            <x-application-logo/>
                        </a>
                        <h1 class="title is-size-1-desktop is-size-3-touch">Se connecter</h1>
                    </div>
                    <div class="card-content">
                        <div class="content">
                            {{ $slot }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    window.current_route = '{{ Route::currentRouteName() }}';
</script>
</body>
</html>
