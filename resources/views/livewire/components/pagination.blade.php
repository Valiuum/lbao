<div>
    @if ($paginator->hasPages())
        <nav class="pagination is-right" role="navigation" aria-label="pagination">
            @if ($paginator->onFirstPage())
                <a class="pagination-previous is-disabled">{{__('messages.previous')}}</a>
            @else
                <a class="pagination-previous"
                   wire:click="previousPage"
                   rel="prev">{{__('messages.previous')}}</a>
            @endif
            @if ($paginator->hasMorePages())
                <a class="pagination-next"
                   wire:click="nextPage"
                   rel="next">
                    {{__('messages.next')}}
                </a>
            @else
                <a class="pagination-next is-disabled">{{__('messages.next')}}</a>
            @endif
            <ul class="pagination-list">
                @foreach ($elements as $element)
                    @if (is_string($element))
                        <li class="pagination-link is-disabled">
                            <span>{{ $element }}</span>
                        </li>
                    @endif
                    @if (is_array($element))
                        @foreach ($element as $page => $url)
                            @if ($page == $paginator->currentPage())
                                <li wire:key="paginator-page-{{ $page }}" class="pagination-link is-current"
                                    aria-label="Page {{$page}}">
                                    {{ $page }}
                                </li>
                            @else
                                <li wire:key="paginator-page-{{ $page }}" class="pagination-link"
                                    wire:click="gotoPage({{ $page }})">
                                    {{ $page }}
                                </li>
                            @endif
                        @endforeach
                    @endif
                @endforeach
            </ul>
        </nav>
    @endif
</div>
