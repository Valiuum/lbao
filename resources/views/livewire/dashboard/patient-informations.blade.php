<div id="dashboard-user" class="is-flex column is-four-fifths-desktop is-full-touch mx-auto is-align-items-center mt-2 mb-5 box {{$patientInformations['username'] ?? 'is-missing'}}" :class="{'is-shadowless': sessionStorage.getItem('darkMode') === 'true' }">
    <div class="column is-three-fifths is-flex is-justify-content-start is-align-items-center">
        <span class="material-symbols-outlined mr-2 is-size-4">account_circle</span>
        @if($patientInformations)
        <p>
            <span class="name">{{ ucfirst($patientInformations['name'][0]) }}.</span>
            <span class="username">{{$patientInformations['username']}}</span>
            <span class="age">{{trans_choice('messages.modal_patient_age_display', \Carbon\Carbon::parse($patientInformations['birthdate'])->age, ['value' => Carbon\Carbon::parse($patientInformations['birthdate'])->age])}}</span>
        </p>
        @else
            <p><span class="username">{{session('patient_username') ?? trans('messages.patient_missing_infos')}}</span></p>
        @endif
    </div>
    <div class="column is-two-fifths is-flex is-justify-content-end">
        <x-button-with-icon class="is-link is-light" icon="edit_document" text="" action="openModal"></x-button-with-icon>
    </div>
</div>
