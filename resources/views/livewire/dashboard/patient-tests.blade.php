<div class="page-content-dashboard-livewire">
    <x-aside-navbar></x-aside-navbar>
    <main id="dashboard-content" class="mx-4 p-4 box is-clipped"
          :class="{'is-shadowless': sessionStorage.getItem('darkMode') === 'true' }">
        @livewire('dashboard.patient-informations')
        <ul x-sort="$wire.sortItem($item, $position)">
            @forelse($patientTestsList as $patientTest)
                <li class="my-4" x-sort:item="'{{$patientTest['uuid']}}'">
                    <livewire:dynamic-component
                        :is="'TestsPatient.' . $patientTest['name']"
                        :id="$patientTest['id']"
                        :uuid="$patientTest['uuid']"
                        key="{{ uniqid('card_') }}"
                    />
                </li>
            @empty
                <li>
                    <div
                        class="box template patient-tests-is-empty is-flex is-align-items-center column is-one-quarter mx-auto is-justify-content-center"
                        :class="{'is-shadowless': sessionStorage.getItem('darkMode') === 'true' }">
                        <img src="{{ asset('images/common/empty.svg') }}" alt="empty" class="is-block mx-auto my-4"/>
                    </div>
                </li>
            @endforelse
        </ul>
        <div class="is-flex is-justify-content-end mt-5">
            @if(collect($patientTestsList)->isNotEmpty())
                <x-button-with-icon
                    class="is-link"
                    icon="docs"
                    text="{{__('messages.generate_report')}}"
                    action="submitItems"
                    disabled="{{ $patientProfileCompleted ? 'false' : 'true' }}">
                </x-button-with-icon>
            @endif
        </div>
    </main>
    <aside id="dashboard-sidebar-right" class="box"
           :class="{'is-shadowless': sessionStorage.getItem('darkMode') === 'true' }">
        <ul>
            <li>
                <button id="dashboard-sidebar-right-btn-toggle" class="is-flex is-align-items-center"
                        @click="dashboard.toggleSidebar('right')">
                    <span class="material-symbols-outlined">keyboard_double_arrow_right</span>
                </button>
            </li>
        </ul>
        <livewire:editor.quill quillReadOnly="true"/>
        <div class="resizer"></div>
    </aside>
</div>
