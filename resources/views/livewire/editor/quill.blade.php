<div wire:ignore>
    @if(Route::currentRouteName() === 'dashboard')
        <div class="box quill-empty mx-auto template">
            <img src="{{ asset('images/common/empty-document.svg') }}" alt="empty" class="is-block mx-auto my-4"/>
        </div>
        <div class="box quill-container is-hidden">
            <div id="{{ $quillId }}" class="m-2">
                {!! $content !!}
            </div>
        </div>
    @elseif(Route::currentRouteName() === 'test.create' || Route::currentRouteName() === 'test.edit')
        <div class="box quill-container">
            <div id="{{ $quillId }}" class="mt-2 border-0">
                {!! $content !!}
            </div>
        </div>
        <button id="insert-table" class="button">Insert Table</button>
    @endif
    @script()
    <script>

        if (current_route === 'test.create' || current_route === 'test.edit') {

            save = function () {
                const content = quill.root.innerHTML
                @this.set('content', content)
            }

        }

        const options = {
            //debug: 'info',
            modules: {
                toolbar: {!! $quillToolBar  !!},
                table: {!! $quillToolBar  !!},
                tableUI: {!! $quillToolBar  !!},
            },
            readOnly: {!! $quillReadOnly  !!},
            theme: 'snow'
        };

        const quill = new Quill('#{{ $quillId }}', options)
        const quillContainer = quill.root.parentElement.parentElement
        const dashboardAsideRight = document.getElementById('dashboard-sidebar-right')
        const dashboardAsideRightButton = document.getElementById('dashboard-sidebar-right-btn-toggle')
        const quillEmpty= document.getElementsByClassName('quill-empty')[0]

        if (current_route === 'test.edit') {

            const table = quill.getModule('table');
            document.querySelector('#insert-table').addEventListener('click', function() {
                table.insertTable(3, 3);
            });

        }

        Livewire.on('quill-content-update', content => {
            if (current_route !== 'dashboard') {
                return
            }
            quill.root.innerHTML = content
            dashboard().quillSidebarContent('show')
        })

        Livewire.on('remove-item', item => {
            if (current_route !== 'dashboard') {
                return
            }
            quill.root.innerHTML = ''
            dashboard().quillSidebarContent('hide')
        })

        Livewire.on('add-item', item => {
            if (current_route !== 'dashboard') {
                return
            }
            dashboard().quillSidebarContent('add')
        })

    </script>
    @endscript
</div>
