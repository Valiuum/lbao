<?php

use App\Livewire\Actions\Logout;

$logout = function (Logout $logout) {
    $logout();
    $this->redirect('/', navigate: true);
};

?>

<nav class="navbar is-fixed-top has-shadow" role="navigation" aria-label="main navigation"
     x-data="{ showLogoutModal: false }">

    <!-- Primary Navigation Menu -->
    <div class="navbar-brand">
        <a href="{{ route('dashboard') }}" wire:navigate class="is-flex is-align-items-center ml-4">
            <x-application-logo/>
        </a>
        <h1 class="navbar-item subtitle is-size-4">
            {{__('messages.website')}}
        </h1>
    </div>

    <!-- Menu Navigation -->
    <div id="navMenu" class="navbar-menu">
        <div class="navbar-start">
            <div class="navbar-item has-dropdown is-hoverable">
                <a class="navbar-link">
                    {{__('messages.handle_dropdown_tests')}}
                </a>
                <div class="navbar-dropdown">
                    <a class="navbar-item" href="{{ route('test.list') }}" wire:navigate>
                        <span class="material-symbols-outlined mr-2">sticky_note_2</span>
                        {{ __('messages.dropdown_tests_my_tests') }}
                    </a>
                    @if(auth()->user()->is_admin())
                        <a class="navbar-item" href="{{ route('category.list') }}" wire:navigate>
                            <span class="material-symbols-outlined mr-2">category</span>
                            {{ __('messages.dropdown_tests_categories') }}
                        </a>
                    @endif
                </div>
            </div>
        </div>
        <div class="navbar-end">
            <!-- Profile -->
            <div class="navbar-item has-dropdown mr-4 is-hoverable">
                <a class="navbar-link is-size-6">
                    <div x-data="{{ json_encode(['name' => auth()->user()->name]) }}"
                         x-text="name"
                         x-on:profile-updated.window="name = $event.detail.name">
                    </div>
                </a>
                <div class="navbar-dropdown is-right">
                    <a class="navbar-item" href="{{route('profile')}}" wire:navigate>
                        <span class="material-symbols-outlined mr-2">face</span>
                        {{ trans('messages.profile') }}
                    </a>
                    @if(auth()->user()->is_admin())
                        <a class="navbar-item" href="{{ route('dashboard') }}" wire:navigate>
                            <span class="material-symbols-outlined mr-2">settings</span>
                            {{__('messages.administration')}}
                        </a>
                    @endif
                    <hr class="navbar-divider">
                    <a class="navbar-item" @click.prevent="showLogoutModal = true">
                        <span class="material-symbols-outlined has-text-danger mr-2">logout</span>
                        {{ trans('messages.logout') }}
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div x-show="showLogoutModal" x-cloak class="modal is-active">
        <div class="modal-background" @click="showLogoutModal = false"></div>
        <div class="modal-content fadeIn">
            <div class="box p-5">
                <div class="columns is-align-items-center p-5">
                    <div class="column is-one-fifths has-text-centered">
                        <span class=" has-text-warning is-size-3 material-symbols-outlined">warning</span>
                    </div>
                    <div class="column is-four-fifths">
                        <p>
                            {{ trans('messages.confirm_logout') }}
                        </p>
                    </div>
                </div>
                <div class="buttons is-right">
                    <button class="button is-danger is-light" @click="$wire.logout()">{{ trans('messages.logout') }}</button>
                    <button class="button is-info is-light" @click="showLogoutModal = false">{{ trans('messages.cancel') }}</button>
                </div>
            </div>
        </div>
    </div>
</nav>
