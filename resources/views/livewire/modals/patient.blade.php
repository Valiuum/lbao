<div id="modal-patient" class="modal {{ $isOpen ? 'is-active' : '' }}">
    <div class="modal-background" wire:click="$set('isOpen', false)"></div>
    <div class="modal-content {{ $isOpen ? 'is-active fadeIn' : 'fadeOut' }}">
        <div class="box p-5">
            <div class="is-flex is-justify-content-end">
                <span wire:click="$set('isOpen', false)" class="delete"></span>
            </div>
            <form wire:submit.prevent="save">
                <div class="field is-horizontal is-align-items-center">
                    <x-input-label :for="'name'" :value="trans('messages.modal_patient_name_username')"/>
                    <div class="field-body">
                        <div class="field column is-half">
                            <div class="control has-icons-left">
                                <x-input-text wire:model="form.name"
                                              placeholder="{{ trans('messages.modal_patient_name_placeholder') }}"
                                              name="name" type="text" class="input" required
                                              autofocus autocomplete="name"/>
                                <x-input-icon class="icon is-small is-left" icon="face"/>
                                @error('form.name')
                                <x-input-error :messages="$message"/>
                                @enderror
                            </div>
                        </div>
                        <div class="field column is-half">
                            <div class="control has-icons-left">
                                <x-input-text wire:model="form.username"
                                              placeholder="{{ trans('messages.modal_patient_username_placeholder') }}"
                                              name="username" type="text" class="input" required
                                              autofocus autocomplete="username"/>
                                <x-input-icon class="icon is-small is-left" icon="face"/>
                                @error('form.username')
                                <x-input-error :messages="$message"/>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="field is-horizontal is-align-items-center">
                    <x-input-label :for="'birthdate'" :value="trans('messages.modal_patient_birthdate')"/>
                    <div class="field-body">
                        <div class="field column is-half is-narrow">
                            <div class="control has-icons-left">
                                <x-input-text wire:model="form.birthdate"
                                              name="birthdate" type="date" class="input" required
                                              autofocus autocomplete="birthdate"/>
                                <x-input-icon class="icon is-small is-left" icon="cake"/>
                                @error('form.birthdate')
                                <x-input-error :messages="$message"/>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="field is-horizontal is-align-items-center">
                    <x-input-label :for="'gender'" :value="trans('messages.modal_patient_gender')"/>
                    <div class="field-body">
                        <div class="field column is-half is-narrow">
                            <div class="control has-icons-left">
                                <div class="select is-fullwidth">
                                    <select wire:model="form.gender">
                                        <option selected>{{ trans('messages.modal_patient_select_gender') }}</option>
                                        @foreach($form->gender_options as $gender)
                                            <option value="{{ $gender }}">{{ trans('messages.' . $gender) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <x-input-icon class="icon is-small is-left" icon="wc"/>
                                @error('form.gender')
                                <x-input-error :messages="$message"/>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="field is-horizontal is-align-items-center">
                    <x-input-label :for="'school_grade'" :value="trans('messages.modal_patient_school_grade')"/>
                    <div class="field-body">
                        <div class="field column is-half is-narrow">
                            <div class="control has-icons-left">
                                <div class="select is-fullwidth">
                                    <select wire:model="form.school_grade">
                                        <option
                                            selected>{{ trans('messages.modal_patient_school_grade_placeholder') }}</option>
                                        @foreach($form->school_grade_options as $grade)
                                            <option value="{{ $grade }}">{{ trans('messages.patient_school_grade_' . $grade) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <x-input-icon class="icon is-small is-left" icon="school"/>
                                @error('form.school_grade')
                                <x-input-error :messages="$message"/>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="field is-horizontal is-align-items-center">
                    <x-input-label :for="'job'" :value="trans('messages.modal_patient_job')"/>
                    <div class="field-body">
                        <div class="field column is-half is-narrow">
                            <div class="control has-icons-left">
                                <x-input-text wire:model="form.job"
                                              name="job" type="text" class="input" required
                                              autofocus autocomplete="job"/>
                                <x-input-icon class="icon is-small is-left" icon="work"/>
                                @error('form.job')
                                <x-input-error :messages="$message"/>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="field is-horizontal is-align-items-center is-justify-content-end"
                     x-data="{darkMode: sessionStorage.getItem('darkMode') === 'true'}">
                    <div class="field is-grouped">
                        <p class="control">
                            <x-primary-button wire:loading.class="is-loading" x-bind:class="{'is-dark': darkMode }" autofocus>
                                {{ trans('messages.save') }}
                            </x-primary-button>
                        </p>
                        <p class="control">
                            <x-danger-button wire:click="clearForm"
                                             x-bind:class="{'is-dark': darkMode }">
                                {{ trans('messages.delete') }}
                            </x-danger-button>
                        </p>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
