<?php

use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Session;

use function Livewire\Volt\layout;
use function Livewire\Volt\rules;
use function Livewire\Volt\state;

layout('layouts.guest');

state(['email' => '']);

rules(['email' => ['required', 'string', 'email']]);

$sendPasswordResetLink = function () {
    $this->validate();

    // We will send the password reset link to this user. Once we have attempted
    // to send the link, we will examine the response then see the message we
    // need to show to the user. Finally, we'll send out a proper response.
    $status = Password::sendResetLink(
        $this->only('email')
    );

    if ($status != Password::RESET_LINK_SENT) {
        $this->addError('email', __($status));

        return;
    }

    $this->reset('email');

    Session::flash('status', __($status));
};

?>

<div>
    <div class="mb-4 text-sm text-gray-600 dark:text-gray-400">
        {{ __('messages.forgot_password_description') }}
    </div>

    <!-- Session Status -->
    <x-auth-session-status class="mb-4" :status="session('status')"/>

    <form wire:submit="sendPasswordResetLink">
        <!-- Email Address -->
        <div class="field">
            <x-input-label for="email"  :value="trans('messages.email')"/>
            <div class="control has-icons-left">
                <x-input-text wire:model="email" id="email" class="input" type="email" name="email" required
                              autofocus/>
                <x-input-icon class="icon is-small is-left" icon="mail"/>
                <x-input-error :messages="$errors->get('email')" class="mt-2"/>
            </div>
        </div>

        <div class="is-flex is-justify-content-center mt-4">
            <x-primary-button class="is-light">
                {{ __('messages.reset_password_send_link') }}
            </x-primary-button>
        </div>
    </form>
</div>
