<?php

use App\Livewire\Forms\LoginForm;
use Illuminate\Support\Facades\Session;
use function Livewire\Volt\form;
use function Livewire\Volt\layout;

layout('layouts.guest');

form(LoginForm::class);

$login = function () {

    $this->validate();

    $this->form->authenticate();

    Session::regenerate();

    $this->redirectIntended(default: route('dashboard', absolute: false), navigate: true);
};

?>

<div>
    <!-- Session Status -->
    <x-auth-session-status class="mb-4" :status="session('status')"/>

    <form wire:submit="login">
        <!-- Email Address -->
        <div class="field">
            <x-input-label for="email" :value="trans('messages.email')"/>
            <div class="control has-icons-left">
                <x-input-text wire:model="form.email" id="email" type="email" name="email"
                              class="{{ $errors->get('form.email') ? 'input is-danger' : '' }}"
                              required autofocus autocomplete="username"
                              placeholder="{{ __('messages.login_placeholder_email') }}"
                />
                <x-input-icon class="icon is-small is-left" icon="mail"/>
                <x-input-error :messages="$errors->get('form.email')"/>
            </div>
        </div>

        <!-- Password -->
        <div class="field mt-4">
            <x-input-label for="password" :value="trans('messages.password')"/>
            <div class="control has-icons-left">
                <x-input-text wire:model="form.password" id="password"
                              class="{{ $errors->get('form.password') ? 'input is-danger' : '' }}"
                              type="password"
                              name="password"
                              required autocomplete="current-password"
                              placeholder="{{ __('messages.login_placeholder_password') }}"
                />
                <x-input-icon class="icon is-small is-left" icon="lock"/>
                <x-input-error :messages="$errors->get('form.password')"/>
            </div>
        </div>

        <!-- Remember Me -->
        <div class="field mt-4">
            <div class="field">
                <label class="b-checkbox checkbox">
                    <input wire:model="form.remember" id="remember" type="checkbox"/>
                    <span class="check is-success"></span>
                    <span class="control-label">{{ __('messages.remember_me') }}</span>
                </label>
            </div>
        </div>

        <div id="buttons-login" class="field is-grouped is-flex is-flex-wrap-wrap">
            <div class="control">
                <x-primary-button class="is-light">
                    {{ __('messages.login') }}
                </x-primary-button>
            </div>
            @if (Route::has('password.request'))
                <div class="control">
                    <a class="button is-info is-light" href="{{ route('password.request') }}" wire:navigate>
                        {{ __('messages.forgot_password') }}
                    </a>
                </div>
            @endif
        </div>
    </form>
    <script>
        window.current_route = '{{ Route::currentRouteName() }}';
    </script>
</div>
