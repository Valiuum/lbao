<?php

use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;

use function Livewire\Volt\layout;
use function Livewire\Volt\rules;
use function Livewire\Volt\state;

layout('layouts.guest');

state([
    'name' => '',
    'email' => '',
    'password' => '',
    'password_confirmation' => ''
]);

rules([
    'name' => ['required', 'string', 'max:255'],
    'email' => ['required', 'string', 'lowercase', 'email', 'max:255', 'unique:' . User::class],
    'password' => ['required', 'string', 'confirmed', Rules\Password::defaults()],
]);

$register = function () {
    $validated = $this->validate();

    $validated['password'] = Hash::make($validated['password']);

    event(new Registered($user = User::create($validated)));

    Auth::login($user);

    $this->redirect(route('home', absolute: false), navigate: true);
};

?>

<div>
    <form wire:submit="register">
        <!-- Name -->
        <div class="field">
            <x-input-label for="name"  :value="__('Name')"/>
            <div class="control has-icons-left">
                <x-input-text wire:model="name" id="name" class="input" type="text" name="name" required autofocus
                              autocomplete="name" placeholder="{{ __('messages.login_placeholder_name') }}"/>
                <x-input-icon class="icon is-small is-left" icon="face"/>
                <x-input-error :messages="$errors->get('name')"/>
            </div>
        </div>

        <!-- Email Address -->
        <div class="field mt-4">
            <x-input-label for="email"  :value="__('Email')"/>
            <div class="control has-icons-left">
                <x-input-text wire:model="email" id="email" class="input" type="email" name="email" required
                              autocomplete="email" placeholder="{{ __('messages.login_placeholder_email') }}"/>
                <x-input-icon class="icon is-small is-left" icon="mail"/>
                <x-input-error :messages="$errors->get('email')"/>
            </div>
        </div>

        <!-- Password -->
        <div class="field mt-4">
            <x-input-label for="password"  :value="__('Password')"/>
            <div class="control has-icons-left">
                <x-input-text wire:model="password" id="password" class="input"
                              type="password"
                              name="password"
                              required autocomplete="new-password" placeholder="{{ __('messages.login_placeholder_password') }}"/>
                <x-input-icon class="icon is-small is-left" icon="lock"/>
                <x-input-error :messages="$errors->get('password')"/>
            </div>
        </div>

        <!-- Confirm Password -->
        <div class="field mt-4">
            <x-input-label for="password_confirmation"  :value="__('Confirm Password')"/>
            <div class="control has-icons-left">
                <x-input-text wire:model="password_confirmation" id="password_confirmation" class="input"
                              type="password"
                              name="password_confirmation" required autocomplete="new-password" placeholder="{{ __('messages.login_placeholder_password_confirm') }}"/>
                <x-input-icon class="icon is-small is-left" icon="lock"/>
                <x-input-error :messages="$errors->get('password_confirmation')"/>
            </div>
        </div>

        <div class="field is-grouped field is-grouped is-flex is-flex-wrap-wrap mt-4">
            <div class="control">
                <a class="button is-link is-light"
                   href="{{ route('login') }}" wire:navigate>
                    {{ __('messages.already_registered') }}
                </a>
            </div>
            <div class="control">
                <x-primary-button class="ms-4">
                    {{ __('messages.register') }}
                </x-primary-button>
            </div>
        </div>
    </form>
</div>
