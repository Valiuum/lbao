<?php

use App\Livewire\Actions\Logout;
use Illuminate\Support\Facades\Auth;

use function Livewire\Volt\rules;
use function Livewire\Volt\state;

state(['password' => '']);

rules(['password' => ['required', 'string', 'current_password']]);

$deleteUser = function (Logout $logout) {
    $this->validate();

    tap(Auth::user(), $logout(...))->delete();

    $this->redirect('/', navigate: true);
};

?>

<section class="box py-5">
    <header>
        <p class="mt-1 mb-3">
            {{ trans('messages.profile_delete_legend') }}
        </p>
    </header>

    <x-danger-button class="my-2" x-data="" x-on:click.prevent="$dispatch('open-modal', 'confirm-user-deletion')">
        {{ trans('messages.profile_delete') }}
    </x-danger-button>

    <x-modal name="confirm-user-deletion" :show="$errors->isNotEmpty()" focusable>
        <form wire:submit="deleteUser" class="p-6">

            <h2 class="has-text-danger has-text-weight-bold">
                {{ trans('messages.profile_delete_final_warning') }}
            </h2>

            <p class="mt-1 has-text-weight-semibold">
                {{ trans('messages.profile_delete_warning') }}
            </p>

            <div class="field mt-5">
                <x-input-label for="password" value="{{ __('messages.login_placeholder_password') }}"/>
                <div class="control has-icons-left">
                    <x-input-text wire:model="password"
                                  id="password"
                                  name="password"
                                  type="password"
                                  class="input"
                                  placeholder="{{ __('Password') }}"
                    />
                    <x-input-icon class="icon is-small is-left" icon="lock"/>
                    <x-input-error :messages="$errors->get('password')" class="mt-2"/>
                </div>
            </div>

            <div class="mt-6 flex justify-end">

                <div class="field is-grouped">
                    <x-danger-button>
                        <p>{{ trans('messages.profile_delete') }}</p>
                    </x-danger-button>
                    <x-secondary-button class="is-light" x-on:click="$dispatch('close')">
                        <p>{{ trans('messages.cancel') }}</p>
                    </x-secondary-button>
                </div>

            </div>
        </form>
    </x-modal>
</section>
