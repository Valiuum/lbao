<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules\Password;
use Illuminate\Validation\ValidationException;

use function Livewire\Volt\rules;
use function Livewire\Volt\state;

state([
    'current_password' => '',
    'password' => '',
    'password_confirmation' => ''
]);

rules([
    'current_password' => ['required', 'string', 'current_password'],
    'password' => ['required', 'string', Password::defaults(), 'confirmed'],
]);

$updatePassword = function () {
    try {
        $validated = $this->validate();
    } catch (ValidationException $e) {
        $this->reset('current_password', 'password', 'password_confirmation');

        throw $e;
    }

    Auth::user()->update([
        'password' => Hash::make($validated['password']),
    ]);

    $this->reset('current_password', 'password', 'password_confirmation');

    $this->dispatch('notify', message: __('Password updated.'));
    $this->dispatch('password-updated');
};

?>

<section class="box py-5">
    <header>
        <h2 class="is-hidden-desktop">
            {{ __('Update Password') }}
        </h2>

        <p class="mt-1">
            {{ trans('messages.profile_password_legend') }}
        </p>
    </header>

    <form wire:submit="updatePassword" class="mt-6">

        <div class="field">
            <x-input-label for="update_password_current_password"  :value="__('messages.login_placeholder_password')"/>
            <div class="control has-icons-left">
                <x-input-text wire:model="current_password" id="update_password_current_password"
                              name="current_password" type="password" class="input"
                              autocomplete="current-password"/>
                <x-input-icon class="icon is-small is-left" icon="password_2"/>
                <x-input-error :messages="$errors->get('current_password')" class="mt-2"/>
            </div>
        </div>

        <div class="field">
            <x-input-label for="update_password_password"  :value="__('messages.login_placeholder_new_password')"/>
            <div class="control has-icons-left">
                <x-input-text wire:model="password" id="update_password_password" name="password" type="password"
                              class="input" autocomplete="new-password"/>
                <x-input-icon class="icon is-small is-left" icon="lock"/>
                <x-input-error :messages="$errors->get('password')" class="mt-2"/>
            </div>
        </div>

        <div class="field">
            <x-input-label for="update_password_password_confirmation"  :value="__('messages.login_placeholder_password_confirm')"/>
            <div class="control has-icons-left">
                <x-input-text wire:model="password_confirmation" id="update_password_password_confirmation"
                              name="password_confirmation" type="password" class="input"
                              autocomplete="new-password"/>
                <x-input-icon class="icon is-small is-left" icon="lock"/>
                <x-input-error :messages="$errors->get('password_confirmation')" class="mt-2"/>
            </div>
        </div>

        <div class="is-flex is-justify-content-flex-end">
            <x-primary-button>{{ trans('messages.save') }}</x-primary-button>
        </div>
    </form>
</section>
