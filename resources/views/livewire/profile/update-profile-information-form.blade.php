<?php

use App\Models\User;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;

use function Livewire\Volt\state;

state([
    'name' => fn() => auth()->user()->name,
    'email' => fn() => auth()->user()->email
]);

$updateProfileInformation = function () {
    $user = Auth::user();

    $validated = $this->validate([
        'name' => ['required', 'string', 'max:255'],
        'email' => ['required', 'string', 'lowercase', 'email', 'max:255', Rule::unique(User::class)->ignore($user->id)],
    ]);

    $user->fill($validated);

    if ($user->isDirty('email')) {
        $user->email_verified_at = null;
    }

    $user->save();

    $this->dispatch('notify', ['message' => trans('messages.profilUpdated'), 'type' => 'success']);
    $this->dispatch('profile-updated', name: $user->name);
};

$sendVerification = function () {
    $user = Auth::user();

    if ($user->hasVerifiedEmail()) {
        $this->redirectIntended(default: route('dashboard', absolute: false));

        return;
    }

    $user->sendEmailVerificationNotification();

    Session::flash('status', 'verification-link-sent');
};

?>

<section class="box py-5">

    <header>
        <h2 class="is-hidden-desktop">
            {{ __('messages.informationProfil') }}
        </h2>

        <p class="mt-1">
            {{ __("messages.profile_account_legend") }}
        </p>
    </header>

    <form wire:submit="updateProfileInformation" class="mt-6">

        <div class="field">
            <x-input-label for="email"  :value="__('messages.login_placeholder_name')"/>
            <div class="control has-icons-left">
                <x-input-text wire:model="name" id="name" name="name" type="text" class="input" required
                              autofocus autocomplete="name"/>
                <x-input-icon class="icon is-small is-left" icon="face"/>
                <x-input-error class="mt-2" :messages="$errors->get('name')"/>
            </div>
        </div>

        <div class="field">
            <x-input-label for="email"  :value="__('messages.login_placeholder_email')"/>
            <div class="control has-icons-left">
                <x-input-text wire:model="email" id="email" name="email" type="email" class="input" required
                              autocomplete="username"/>
                <x-input-icon class="icon is-small is-left" icon="email"/>
                <x-input-error class="mt-2" :messages="$errors->get('email')"/>
            </div>
            @if (auth()->user() instanceof MustVerifyEmail && ! auth()->user()->hasVerifiedEmail())
                <div class="field is-grouped">
                    <div class="control">
                        <p class="mt-2">
                            {{ __('Your email address is unverified.') }}
                            <button wire:click.prevent="sendVerification"
                                    class="button is-link">
                                {{ __('Click here to re-send the verification email.') }}
                            </button>
                        </p>
                    </div>
                    @if (session('status') === 'verification-link-sent')
                        <p class="mt-2">
                            {{ __('A new verification link has been sent to your email address.') }}
                        </p>
                    @endif
                </div>
            @endif
        </div>

        <div class="is-flex is-justify-content-flex-end">
            <x-primary-button>{{ trans('messages.save') }}</x-primary-button>
        </div>

    </form>

</section>
