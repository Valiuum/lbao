<div class="{{!$displayTable ? 'column is-12' : ''}}">
    <form
        wire:submit="save" @class(['box' => !$displayTable, 'column'])>
        <div @class(['m-4' => !$displayTable, 'p-4' => !$displayTable, 'my-4' => $displayTable, 'field', 'is-horizontal'])">
            <x-input-label class="is-pulled-right" for="name" :value="__('messages.name')"/>
            <div class="field-body">
                <div class="field">
                    <div class="control has-icons-left">
                        <input class="input" type="text" wire:model="form.name">
                        <x-input-icon class="icon is-small is-left" icon="description"/>
                        <x-input-error :messages="$errors->get('form.name')" class="mt-2"/>
                    </div>
                </div>
                <div class="field">
                    <p class="control">
                        <button class="button is-link">{{ __('messages.save') }}</button>
                    </p>
                </div>
            </div>
        </div>
    </form>
</div>
