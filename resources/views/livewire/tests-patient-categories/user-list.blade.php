<div class="container" x-data="{selection: $wire.entangle('selection')}">
    <div class="columns is-align-items-center is-multiline is-mobile">
        <div class="column is-full">
            <x-link-button
                wire:navigate
                href="{{ route('category.create') }}">
                {{ __('messages.category_create') }}
            </x-link-button>
        </div>
        <div class="column is-full">
            <div class="is-flex is-justify-content-center">
                <label for="search_ingredient"
                       class="is-size-4 has-text-weight-semibold">{{__('messages.category_search')}}</label>
            </div>
            <div class="column is-one-third-desktop is-half-tablet mx-auto px-0">
                <div id="tableSearchActivities"
                     class="tableSearch control field mb-5 mx-auto has-icons-left">
                    <x-input-text
                        wire:model.live.debounce.250ms="search"
                        id="search_category"
                        class="input is-half-desktop"
                        name="search_category"
                        type="text"
                    />
                    <x-input-icon class="icon is-small is-left" icon="search"/>
                </div>
            </div>
            <x-danger-button
                class="button is-danger is-light my-4"
                x-cloak
                x-show="selection.length > 0"
                x-on:click="$wire.deleteSelected">
                <span class="material-symbols-outlined mr-2">delete</span>
                <span>{{ __('messages.delete') }}</span>
            </x-danger-button>
            <div class="table-container">
                <table class="table is-fullwidth is-hoverable">
                    <thead>
                    <tr>
                        <th>{{__('messages.select')}}</th>
                        <x-table-header name="name" :direction="$orderDirection" :field="$orderField">
                            {{__('messages.name')}}
                        </x-table-header>
                        <th>{{__('messages.edit')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($categories as $category)
                        <tr>
                            <td>
                                <div class="field ml-2">
                                    <label class="b-checkbox checkbox">
                                        <input id="checkbox-{{$category->id}}"
                                               class="is-checkradio is-circle"
                                               x-model="selection"
                                               value="{{$category->id}}"
                                               type="checkbox"
                                               name="checkbox-{{$category->id}}">
                                        <span class="check is-link"></span>
                                    </label>
                                </div>
                            </td>
                            <td>{{ $category->name }}</td>
                            <td>
                                <button
                                    class="button is-link is-light"
                                    :class="{'is-dark': sessionStorage.getItem('darkMode') === 'true' }"
                                    wire:click="startEdit({{$category->id}})">
                                    <span class="material-symbols-outlined">edit</span>
                                </button>
                            </td>
                        </tr>
                        @if($editId === $category->id)
                            <tr class="is-active">
                                <td colspan="3">
                                    <livewire:tests-patient-categories.user-list-edit
                                        :displayTable="true"
                                        :edit="false"
                                        :category="$category"
                                        :key="$category->id"/>
                                </td>
                            </tr>
                    @endif
                    @endforeach
                </table>
            </div>
            {{$categories->links()}}
        </div>
    </div>
</div>
