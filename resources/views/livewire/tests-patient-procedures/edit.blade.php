<div class="container">
    <div class="columns">
        <div class="column is-full">
            <div class="box" :class="{'is-shadowless': sessionStorage.getItem('darkMode') === 'true' }">
                <div class="columns is-mobile is-multiline">
                    <div
                        class="column is-flex is-justify-content-center is-two-thirds-desktop is-half-tablet is-full-mobile">
                        <div class="field is-grouped-multiline is-fullwidth">
                            <x-input-label for="name" :value="__('messages.name')"/>
                            <div class="control has-icons-left">
                                <input class="input" type="text" wire:model="form.name">
                                <x-input-icon class="icon is-small is-left" icon="description"/>
                                <x-input-error :messages="$errors->get('form.name')" class="mt-2"/>
                            </div>
                        </div>
                    </div>
                    <div
                        class="column is-flex is-align-items-center is-justify-content-center is-one-third-desktop is-half-tablet is-full-mobile">
                        <div class="control">
                            <label class="b-checkbox checkbox">
                                <input wire:model="form.is_active" type="checkbox"/>
                                <span class="check is-link"></span>
                                <span
                                    class="control-label tag is-link is-light ml-2">{{ __('messages.test_select_actif') }}</span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="columns is-mobile is-multiline">
                    <div
                        class="column is-flex is-justify-content-center is-one-third-desktop is-half-tablet is-full-mobile">
                        <div class="field is-grouped-multiline is-fullwidth">
                            <x-input-multi-select
                                :model="'optionsUsers'"
                                :options-values="\App\Models\User::all()->select('name', 'id')"
                                :options-list="$optionsUsers"
                                label="{{ trans_choice('messages.test_users_label', $test->users->count()) }}"
                                placeholder="{{ trans('messages.test_user_placeholder') }}"
                            />
                            @tablet
                            <x-input-multi-select
                                :model="'optionsList'"
                                :options-values="$categories"
                                :options-list="$optionsList"
                                label="{{ trans_choice('messages.test_categories_label', $test->categories->count()) }}"
                                placeholder="{{ trans('messages.test_category_placeholder') }}"
                            />
                            @error('form.categories')
                            <p class="help is-danger m-2">{{ $message }}</p>
                            @enderror
                            @endtablet
                            @error('form.users')
                            <p class="help is-danger m-2">{{ $message }}</p>
                            @enderror
                        </div>
                    </div>
                    @desktop
                    <div
                        class="column is-flex is-justify-content-center is-one-third-desktop is-half-tablet is-full-mobile">
                        <div class="field is-fullwidth">
                            <x-input-multi-select
                                :model="'optionsList'"
                                :options-values="$categories"
                                :options-list="$optionsList"
                                label="{{ trans_choice('messages.test_categories_label', $test->categories->count()) }}"
                                placeholder="{{ trans('messages.test_category_placeholder') }}"
                            />
                            @error('form.categories')
                            <p class="help is-danger m-2">{{ $message }}</p>
                            @enderror
                        </div>
                    </div>
                    @enddesktop
                    <div
                        class="column is-flex is-justify-content-center is-one-third-desktop is-half-tablet is-full-mobile">
                        <div class="field is-fullwidth">
                            <x-input-create-tags
                                :options-values="$optionsVersions"
                                :model="'optionsVersions'"
                                label="{{ trans_choice('messages.test_versions_label', collect($optionsVersions)->count()) }}"
                            />
                            @error('form.versions')
                            <p class="help is-danger m-2">{{ $message }}</p>
                            @enderror
                        </div>
                    </div>
                </div>

            </div>

            @php
                $content = $test->content ?? trans('messages.test_content_placeholder');
            @endphp

            <livewire:editor.quill :content="$content"/>

            <div class="field is-grouped mt-4 is-justify-content-end">
                <div class="control">
                    <a href="{{route('test.list')}}" class="button is-link is-light" wire:navigate>
                        {{ __('messages.cancel') }}
                    </a>
                </div>
                <div class="control">
                    <x-primary-button @click="save" wire:loading.class="is-loading">
                        {{ __('messages.save') }}
                    </x-primary-button>
                </div>
            </div>

        </div>
    </div>
</div>
