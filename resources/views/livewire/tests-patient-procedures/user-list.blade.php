<div class="container">
    <div class="columns is-align-items-center is-multiline is-mobile">
        <div class="column is-full">
            <x-link-button
                wire:navigate
                href="{{ route('test.create') }}">
                {{ __('messages.test_create') }}
            </x-link-button>
        </div>
        @forelse($tests as $test)
            <div class="column is-one-quarter-desktop is-half-tablet is-full-mobile">
                <div class="card">
                    <header class="card-header">
                        <p class="card-header-title test-name">{{$test->name}}</p>
                        <p class="tag m-3 {{$test->is_active ? 'is-success is-light' : 'is-danger is-light'}} card-header-title test-visibility">
                            <span class="material-symbols-outlined">
                                {{$test->is_active ? 'visibility' : 'visibility_off'}}
                            </span>
                        </p>
                    </header>
                    <div class="card-content">
                        <div class="content">
                            @foreach($test->categories as $category)
                                <a class="tag my-1">{{$category->name}}</a>
                            @endforeach
                            <p class="mt-2">{{ trans_choice('messages.test_modification_date', 1, ['value' => \Carbon\Carbon::parse($test->updated_at)->diffForHumans()]) }}</p>
                        </div>
                    </div>
                    <footer class="card-footer">
                        <a href="{{ route('test.edit', $test) }}" wire:navigate
                           class="card-footer-item has-text-link">{{ __('messages.edit') }}</a>
                        <form class="card-footer-item" action="{{ route('test.destroy', $test) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="has-text-danger">{{ __('messages.delete') }}</button>
                        </form>
                    </footer>
                </div>
            </div>
        @empty
            <div class="box template patient-tests-is-empty is-flex is-align-items-center column is-2 mx-auto is-justify-content-center">
                <img src="{{ asset('images/common/empty-document.svg') }}" alt="empty"
                     class="is-block mx-auto my-4"/>
            </div>
        @endforelse
    </div>
    <div class="column is-full">
        {{ $tests->links() }}
    </div>
</div>



