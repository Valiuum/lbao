<div id="{{$uuid}}" class="box item has-background-white-bis is-clipped"
     x-data="{ expanded: false, form: @entangle('form')}"
     :class="{'is-shadowless': sessionStorage.getItem('darkMode') === 'true' }">
    <x-dashboard-test-header :id="$test->id" :uuid="$uuid" :name="$name" :form="$form"></x-dashboard-test-header>
    <div x-show="expanded" x-cloak x-collapse>
        <form wire:submit.prevent="save">
            @if ($errors->any)
                <div class="field">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li class="tag is-danger is-light m-1">{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="field">
                <x-timer/>
            </div>
            <div class="field is-grouped">
                <div class="columns column is-multiline is-mobile is-clipped">
                    <div
                        class="column is-one-third-desktop is-full-mobile is-flex is-align-items-center is-clipped has-background-success">
                        <div class="columns column is-multiline is-mobile is-clipped">
                            <div
                                class="column is-three-fifths-desktop is-full-touch fcr-image-container is-clipped is-flex is-justify-content-center has-background-success-light">
                                <figure class="image is-4by3 has-background-warning-light">
                                    <img alt="picture" src="{{ asset('images/tests/fcr/figure_a/face.svg') }}"/>
                                </figure>
                            </div>
                            <div
                                class="column is-two-fifths-desktop is-full-touch is-flex is-align-items-center is-clipped is-justify-content-center has-background-success-invert">
                                <div class="field">
                                    <x-input-label for="update_password_current_password"
                                                   :value="__('messages.fcr_score')"/>
                                    <div class="control">
                                        <x-input-text wire:model="current_password"
                                                      id="update_password_current_password"
                                                      name="current_password" type="number" class="input"
                                                      autocomplete="current-password"/>
                                    </div>
                                </div>
                            </div>
                            <x-input-error :messages="$errors->get('current_password')" class="mt-2"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="field">
                <textarea class="textarea" x-model="form.comment" name="comment"
                          placeholder="{{__('messages.add_comment')}}"></textarea>
                <x-input-error :messages="$errors->get('form.comment')" class="mt-2"/>
            </div>
            <div class="field is-flex is-justify-content-end">
                <div class="control">
                    <x-primary-button wire:loading.class="is-loading">
                        {{ trans('messages.save') }}
                    </x-primary-button>
                </div>
            </div>
        </form>
    </div>
</div>
