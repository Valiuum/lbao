<div id="{{$uuid}}" class="box item has-background-white-bis is-clipped" x-data="{ expanded: false, form: @entangle('form')}"
     :class="{'is-shadowless': sessionStorage.getItem('darkMode') === 'true' }">
    <x-dashboard-test-header :id="$test->id" :uuid="$uuid" :name="$name" :form="$form"></x-dashboard-test-header>
    <div x-show="expanded" x-cloak x-collapse>
        <form wire:submit.prevent="save">
            @if ($errors->any)
                <div class="field">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li class="tag is-danger is-light m-1">{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="field">
                <x-timer/>
            </div>
            <div class="table-container">
                <table class="table is-fullwidth mcst is-hoverable has-sticky-header">
                    <colgroup class="table_mcst_card" span="2"></colgroup>
                    <colgroup class="table_mcst_criteria" span="4"></colgroup>
                    <colgroup class="table_mcst_answer" span="3"></colgroup>
                    <thead>
                    <tr>
                        <th colspan="1"></th>
                        <th colspan="1"></th>
                        <th class="has-text-centered" colspan="4">{{__('messages.mcst_table_header_criteria')}}</th>
                        <th class="has-text-centered table_mcst_answer"
                            colspan="3">{{__('messages.mcst_table_header_answer')}}</th>
                    </tr>
                    <tr>
                        <th class="has-text-centered">{{__('messages.mcst_table_header_card_position')}}</th>
                        <th class="has-text-centered">{{__('messages.mcst_table_header_card')}}</th>
                        <th class="has-text-centered">{{__('messages.mcst_table_header_color')}}</th>
                        <th class="has-text-centered">{{__('messages.mcst_table_header_shape')}}</th>
                        <th class="has-text-centered">{{__('messages.mcst_table_header_number')}}</th>
                        <th class="has-text-centered">{{__('messages.mcst_table_header_other')}}</th>
                        <th class="has-text-centered table_mcst_answer">{{__('messages.mcst_table_header_+')}}</th>
                        <th class="has-text-centered table_mcst_answer">{{__('messages.mcst_table_header_e')}}</th>
                        <th class="has-text-centered table_mcst_answer">{{__('messages.mcst_table_header_ep')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($form->mcst as $key => $card)
                        <tr>
                            <td>{{ $card['position'] }}</td>
                            <td>{{ $card['card'] }}</td>
                            <td>
                                <label class="b-radio radio">
                                    <input type="radio" name="{{ $key }}.criteria"
                                           x-model="form.mcst.{{ $key }}.criteria" value="color"/>
                                    <span class="check"></span>
                                </label>
                            </td>
                            <td>
                                <label class="b-radio radio">
                                    <input type="radio" name="{{ $key }}.criteria"
                                           x-model="form.mcst.{{ $key }}.criteria" value="shape"/>
                                    <span class="check"></span>
                                </label>
                            </td>
                            <td>
                                <label class="b-radio radio">
                                    <input type="radio" name="{{ $key }}.criteria"
                                           x-model="form.mcst.{{ $key }}.criteria" value="number"/>
                                    <span class="check"></span>
                                </label>
                            </td>
                            <td>
                                <div class="field is-grouped is-flex is-align-items-center is-justify-content-center">
                                    <div class="control">
                                        <label class="b-radio radio">
                                            <input type="radio" name="{{ $key }}.criteria"
                                                   x-model="form.mcst.{{ $key }}.criteria" value="other"/>
                                            <span class="check"></span>
                                        </label>
                                    </div>
                                    <div class="control">
                                        <input class="input" type="text" name="{{ $key }}.criteria_reason"
                                               x-model="form.mcst.{{ $key }}.criteria_reason"/>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <label class="b-radio radio">
                                    <input type="radio" name="{{ $key }}.answer"
                                           x-model="form.mcst.{{ $key }}.answer" value="+"
                                           x-on:change="mcst().tableAnswer($data)"/>
                                    <span class="check is-info"></span>
                                </label>
                            </td>
                            <td>
                                <label class="b-radio radio">
                                    <input type="radio" name="{{ $key }}.answer"
                                           x-model="form.mcst.{{ $key }}.answer" value="e"
                                           x-on:change="mcst().tableAnswer($data)"/>
                                    <span class="check is-info"></span>
                                </label>
                            </td>
                            <td>
                                <label class="b-radio radio">
                                    <input type="radio" name="{{ $key }}.answer"
                                           x-model="form.mcst.{{ $key }}.answer" value="ep"
                                           x-on:change="mcst().tableAnswer($data)"/>
                                    <span class="check is-info"></span>
                                </label>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="field">
                <x-input-label for="nbr_categories_completed"
                               :value="__('messages.mcst_nbr_categories_completed')"/>
                <div class="control has-icons-left">
                    <x-input-text x-model="form.mcst_results.nbr_categories_completed.score"
                                  name="nbr_categories_completed" type="number" class="input"/>
                    <x-input-icon class="icon is-small is-left" icon="check"/>
                    <x-input-error :messages="$errors->get('form.mcst_results.nbr_categories_completed.score')" class="mt-2"/>
                </div>
            </div>
            <div class="field">
                <x-input-label for="nbr_errors_no"
                               :value="__('messages.mcst_nbr_errors_no')"/>
                <div class="control has-icons-left">
                    <x-input-text x-model="form.mcst_results.nbr_errors_no.score"
                                  name="nbr_errors_no" type="number" class="input"/>
                    <x-input-icon class="icon is-small is-left" icon="error"/>
                    <x-input-error :messages="$errors->get('form.mcst_results.nbr_errors_no.score')" class="mt-2"/>
                </div>
            </div>
            <div class="field">
                <x-input-label for="nbr_perseverations"
                               :value="__('messages.mcst_nbr_perseverations')"/>
                <div class="control has-icons-left">
                    <x-input-text x-model="form.mcst_results.nbr_perseverations.score"
                                  name="nbr_perseverations" type="number" class="input"/>
                    <x-input-icon class="icon is-small is-left" icon="warning"/>
                    <x-input-error :messages="$errors->get('form.mcst_results.nbr_perseverations.score')" class="mt-2"/>
                </div>
            </div>
            <div class="field">
                <x-input-label for="percentage_perseverations"
                               :value="__('messages.mcst_percentage_perseverations')"/>
                <div class="control has-icons-left">
                    <x-input-text x-model="form.mcst_results.percentage_perseverations.score"
                                  name="percentage_perseverations" type="number" step="0.01" class="input"/>
                    <x-input-icon class="icon is-small is-left" icon="percent"/>
                    <x-input-error :messages="$errors->get('form.mcst_results.percentage_perseverations.score')" class="mt-2"/>
                </div>
            </div>
            <div class="field">
                <x-input-label for="nbr_cards_six_cat"
                               :value="__('messages.mcst_nbr_cards_six_cat')"/>
                <div class="control has-icons-left">
                    <x-input-text x-model="form.mcst_results.nbr_cards_six_cat.score"
                                  name="nbr_cards_six_cat" type="number" class="input"/>
                    <x-input-icon class="icon is-small is-left" icon="pin"/>
                    <x-input-error :messages="$errors->get('form.mcst_results.nbr_cards_six_cat.score')" class="mt-2"/>
                </div>
            </div>
            <div class="field">
                <x-input-label for="nbr_dropouts_premature"
                               :value="__('messages.mcst_nbr_dropouts_premature')"/>
                <div class="control has-icons-left">
                    <x-input-text x-model="form.mcst_results.nbr_dropouts_premature.score"
                                  name="nbr_dropouts_premature.score" type="number" class="input"/>
                    <x-input-icon class="icon is-small is-left" icon="login"/>
                    <x-input-error :messages="$errors->get('form.mcst_results.nbr_dropouts_premature.score')" class="mt-2"/>
                </div>
            </div>
            <div class="field">
                <x-input-label for="mcst_time"
                               :value="__('messages.mcst_time')"/>
                <div class="control has-icons-left">
                    <x-input-text x-model="form.time"
                                  name="mcst_time" type="time" step="1" class="input"/>
                    <x-input-icon class="icon is-small is-left" icon="timelapse"/>
                    <x-input-error :messages="$errors->get('form.time')" class="mt-2"/>
                </div>
            </div>
            <div class="field">
                <textarea class="textarea" x-model="form.comment" name="comment"
                          placeholder="{{__('messages.add_comment')}}"></textarea>
                <x-input-error :messages="$errors->get('form.comment')" class="mt-2"/>
            </div>
            <div class="field is-flex is-justify-content-end">
                <div class="control">
                    <x-primary-button wire:loading.class="is-loading">
                        {{ trans('messages.save') }}
                    </x-primary-button>
                </div>
            </div>
        </form>
    </div>
</div>
