<div id="{{$uuid}}" class="box item has-background-white-bis" x-data="{ expanded: false, form: @entangle('form')}"
     :class="{'is-shadowless': sessionStorage.getItem('darkMode') === 'true' }">
    <x-dashboard-test-header :id="$test->id" :uuid="$uuid" :name="$name"></x-dashboard-test-header>
    <p>{{$uuid}}</p>
    <div x-show="expanded" x-cloak x-collapse>
        <form wire:submit.prevent="save" id="form">
            <div class="field">
                <label class="label">Name</label>
                <div class="control">
                    <input class="input" type="text" x-model="form.username" placeholder="Text input"
                           x-on:input="dashboard.updateUsername($data,$event.target.value)">
                </div>
            </div>
            <div class="field">
                <label class="label">Name</label>
                <div class="control">
                    <input class="input" type="text" x-model="form.password" placeholder="Text input">
                </div>
            </div>
            <div class="field">
                <label class="label">Name</label>
                <div class="control">
                    <input class="input" type="text" x-model="form.value" placeholder="Text input">
                </div>
            </div>


            <div class="field is-flex is-justify-content-end">
                <div class="control">
                    <x-primary-button wire:loading.class="is-loading">
                        {{ trans('messages.save') }}
                    </x-primary-button>
                </div>
            </div>
        </form>
    </div>
    @once
        <script>

            window.rlriForm = () => {
                return {
                    updateUsername($data,value) {
                        console.log('Current letter count:', value.length);
                        this.$data.form.value = value.length;
                        //console.log('Updated value:', value);
                        //console.log('Current value:', this.form.value);
                        //window.Livewire.find(document.getElementById('form').closest('[wire\\:id]').getAttribute('wire:id')).set('form.value', value.length);
                    }
                }
            }

            /*const uuid = document.getElementById('{{$uuid}}').getAttribute('id');
            function updateUsername(value) {
                console.log('UUID:', uuid);
                //console.log('Current letter count:', value.length);
                //console.log('Updated value:', value);
                //console.log('Current value:', document.getElementById('form').closest('{{ $uuid }}').getAttribute('wire:id'));
                //window.Livewire.find(document.getElementById('form').closest('[wire\\:id]').getAttribute('wire:id')).set('form.value', value.length);
            }*/
        </script>
    @endonce
</div>
