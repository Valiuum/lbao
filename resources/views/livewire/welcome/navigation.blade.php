<nav class="has-text-centered">
    @auth
        <a href="{{ url('dashboard') }}" class="button is-link is-light">
            {{__('messages.home')}}
        </a>
    @else
        <a href="{{ route('login') }}" class="button is-link is-light">
            {{__('messages.login')}}
        </a>

        @if (Route::has('register'))
            <a href="{{ route('register') }}" class="button is-info is-light">
                {{__('messages.register')}}
            </a>
        @endif
    @endauth
</nav>
