<x-app-layout>

    <x-slot name="header" :imgPath="asset('images/header/profile.svg')"></x-slot>
    <section class="@desktop px-5 @elsedesktop px-2 @enddesktop">
        <div class="columns profils-columns is-multiline is-mobile">
            <div class="column is-12-touch is-4-desktop py-5 is-hidden-touch">
                <h6 class="is-inline-flex is-size-6 has-text-weight-semibold profile-header px-4 py-2 has-background-link-light has-text-link">{{__('messages.profile_account')}}</h6>
            </div>

            <div class="column is-12-touch is-8-desktop">
                <livewire:profile.update-profile-information-form/>
            </div>

            <div class="column is-12-touch is-4-desktop py-5 is-hidden-touch">
                <h6 class="is-inline-flex is-size-6 has-text-weight-semibold profile-header px-4 py-2 has-background-link-light has-text-link">{{__('messages.profile_password')}}</h6>
            </div>

            <div class="column is-12-touch is-8-desktop">
                <livewire:profile.update-password-form/>
            </div>

            <div class="column is-12-touch is-4-desktop py-5 is-hidden-touch">
                <h6 class="is-inline-flex is-size-6 has-text-weight-semibold profile-header px-4 py-2 has-background-link-light has-text-link">{{__('messages.profile_delete')}}</h6>
            </div>

            <div class="column is-12-touch is-8-desktop profile-delete-account">
                <livewire:profile.delete-user-form/>
            </div>

        </div>

    </section>

</x-app-layout>
