<x-app-layout>
    <div id="categories-content" class="container">
        <div class="columns is-align-items-center is-multiline is-mobile">
            <div class="column is-full">
                <h4 class="is-size-4 m-2"> {{ trans('messages.category_create') }}</h4>
                <livewire:tests-patient-categories.user-list-edit
                    :displayTable="false"
                    :edit="true"
                    :category="$category"
                    :key="$category->id"/>
            </div>
        </div>
    </div>
</x-app-layout>
