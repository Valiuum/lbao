<x-app-layout>
    <div id="test-content">
        <div class="columns is-align-items-center is-multiline is-mobile">
            <div class="column is-full">
                <h4 class="is-size-4 m-2"> {{ trans('messages.test_create') }}</h4>
                @livewire('tests-patient-procedures.edit')
            </div>
        </div>
    </div>
</x-app-layout>
