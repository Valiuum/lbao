<x-app-layout>
    <div id="editor-content">
        @livewire('tests-patient-procedures.edit' , ['test' => $test])
    </div>
</x-app-layout>
