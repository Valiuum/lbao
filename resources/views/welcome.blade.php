<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>La boite à outils</title>

    <meta name="mobile-web-app-capable" content="yes">
    <meta name="description" content="La boite à outils">

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('site.webmanifest')}}">
    <link rel="mask-icon" href="{{asset('safari-pinned-tab.svg')}}" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet"/>
    <!-- Styles -->
    @vite(['resources/css/app.scss'])
</head>
<body class="guest">
<div class="gradient"></div>
<section class="hero is-fullheight">
    <div class="hero-body m-auto">
        <div class="columns mx-auto is-centered">
            <div class="column is-12-touch is-8-tablet is-10-desktop m-auto">
                <div class="card">
                    <div class="card-header">
                        <x-application-logo class="mx-auto is-flex mt-3 is-64x64"/>
                    </div>
                    <div class="card-content has-text-centered">
                        <h1 class="title is-size-1-desktop is-size-3-touch">{{__('messages.website')}}</h1>
                        <div class="content">
                            @if (Route::has('login'))
                                <livewire:welcome.navigation/>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</body>
</html>
