<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\PdfController;
use App\Http\Controllers\TestController;
use Illuminate\Support\Facades\Route;

Route::view('/', 'welcome');

Route::view('dashboard', 'dashboard')
    ->middleware(['auth', 'verified'])
    ->name('dashboard');

Route::view('profile', 'profile')
    ->middleware(['auth'])
    ->name('profile');

// Tests
Route::prefix('tests')->name('test.')->middleware(['auth', 'verified'])->group(function () {
    Route::get('create', [TestController::class, 'create'])
        ->name('create');
    Route::get('list', [TestController::class, 'list'])
        ->name('list');
    Route::get('show/{test}', [TestController::class, 'show'])
        ->name('show');
    Route::get('edit/{test}', [TestController::class, 'edit'])
        ->name('edit');
    Route::delete('destroy/{test}', [TestController::class, 'destroy'])
        ->name('destroy');
});

// Categories
Route::prefix('categories')->name('category.')->middleware(['auth', 'verified', 'is_admin'])->group(function () {
    Route::get('create', [CategoryController::class, 'create'])
        ->name('create');
    Route::get('list', [CategoryController::class, 'list'])
        ->name('list');
    Route::get('show/{category}', [CategoryController::class, 'show'])
        ->name('show');
    Route::get('edit/{category}', [CategoryController::class, 'edit'])
        ->name('edit');
    Route::delete('destroy/{category}', [CategoryController::class, 'destroy'])
        ->name('destroy');
});

// PDF
Route::prefix('pdf')->name('pdf.')->middleware(['auth', 'verified', 'tests_completed'])->group(function () {
    Route::get('generate', [PdfController::class, 'generate'])
        ->name('generate');
});

require __DIR__.'/auth.php';
