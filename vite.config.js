import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';
import postcss from '@vituum/vite-plugin-postcss';
import purge from '@erbelion/vite-plugin-laravel-purgecss'

export default defineConfig({
    plugins: [
        laravel({
            input: [
                'resources/js/app.js',
                'resources/js/dashboard.js',
                'resources/js/timer.js',
                'resources/js/guest.js',
                'resources/css/app.scss',
            ],
            refresh: true,
        }),
        postcss(),
        purge(),
    ],
    css: {
        devSourcemap: true,
    },
});
